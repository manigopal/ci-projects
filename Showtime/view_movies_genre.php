
<!-- /w3l-medile-movies-grids -->
	<div class="general-agileits-w3l">
		<div class="w3l-medile-movies-grids">

				<!-- /movie-browse-agile -->
				
				      <div class="movie-browse-agile">
					     <!--/browse-agile-w3ls -->
						<div class="browse-agile-w3ls general-w3ls">
								<div class="tittle-head">
								
								<h4 class="latest-text">
									<?php //echo $movies_fetched->movie_id;?> 
									<?php //echo $movies_by_genre->movie_id; ?>
									<?php //echo $genre_type; ?>
									<?php //echo $genre_by_name['genre_type'] ; ?>
									<?php echo $genre_name; ?>
								</h4>
									<div class="container">
										<div class="agileits-single-top">
											<ol class="breadcrumb">
											
											  <li><a href="<?php echo base_url(); ?>">Home</a></li>
											  <li class="active" style="text-transform:Capitalize;">
											  	<?php //echo $movies_fetched->movie_genre;?>
											  	<?php //echo $genre_by_name['genre_type'] ; ?>
											  	<?php echo $genre_name; ?>
											  </li>
											</ol>
										</div>
									</div>
								</div>
								     <div class="container">
							<div class="browse-inner">
								
								<?php 
								//foreach($movies_fetched as $row) {  ?>
								<?php if ($total_rows>0):?>
								<?php foreach ($all_published_videos as $row): ?>	
							?>	
							   <div class="col-md-2 w3l-movie-gride-agile"> 
										 <a href="<?php echo base_url(); ?>movie/<?php echo $row->movie_id; ?>" class="hvr-shutter-out-horizontal">
										 	<img src="<?php echo base_url(); ?>/public/images/movies/<?php echo $row->movie_image; ?>" title="<?php echo $row->movie_name; ?>" alt=" " />
									     <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
									</a>
									  <div class="mid-1">
										<div class="w3l-movie-text">
											<h6><a href="<?php echo base_url(); ?>movie/<?php echo $row->movie_id; ?>">
												<?php //echo $row->movie_name; ?>
													<?php echo $row['movie_name']; ?>
												</a></h6>							
										</div>
										<div class="mid-2">
										
											<p><?php echo $row->release_year; ?></p>
											<div class="block-stars">
												<ul class="w3l-ratings">
													     
														 <li> 
														  <span>IMDB <i class="fa fa-star" aria-hidden="true"></i> 
														  	<?php echo $row->movie_rating; ?> 
														  </span>
														 </li>
										
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
											
									</div>
							 	    <div class="ribben two">
										<p>NEW</p>
									</div>	
									</div> 
								<?php //} ?>
								<?php endforeach; ?>
									
									<div class="clearfix"> </div>
								</div>
								
							
								</div>
						</div>
				<!--//browse-agile-w3ls -->
						<!-- <div class="blog-pagenat-wthree">
							<ul> -->										
									
									<?php
									// display the links to the pages
									/*for($page=1;$page<=$number_of_pages;$page++){*/
										//echo '<a href="movies.php?page=' . $page . '">' . $page . '</a> ';
									
								/* <li><a class="frist" href="#">Prev</a></li> */
									/*echo '<li><a href="movies-genre.php?genre_type='.$_REQUEST['genre_type'].'&page=' . $page . '">' . $page . '</a></li>';
								}*/
									 //echo $links;
									?>
								<!--<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a class="last" href="#">Next</a></li> -->
							<!-- </ul>
						</div> -->

						<?php else: echo "
						<div class='row'>
						<h3 class='text-center text-capitalize'>No Movie Found by ".$genre_name."</h3> </div>"; endif; ?>
						 <?php if($total_rows > 24): echo $links;endif;?>
					</div> 
				    <!-- //movie-browse-agile -->
				
		</div>
	<!-- //w3l-medile-movies-grids -->
	</div>
	<!-- //comedy-w3l-agileits