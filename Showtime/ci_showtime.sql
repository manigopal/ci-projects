-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2019 at 07:08 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_showtime`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners_info`
--

CREATE TABLE `banners_info` (
  `banner_id` int(11) NOT NULL,
  `banner_title` varchar(255) NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  `banner_description` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners_info`
--

INSERT INTO `banners_info` (`banner_id`, `banner_title`, `banner_image`, `banner_description`, `status`) VALUES
(1, 'Tarzan', 'tarzan-banner.jpg', 'Tarzan, having acclimated to life in London, is called back to his former home in the jungle to investigate the activities at a mining encampment.', 'Yes'),
(2, 'Maximum Ride', '2.jpg', 'Six children, genetically cross-bred with avian DNA, take flight around the country to discover their origins. Along the way, their mysterious past is ...', 'Yes'),
(3, 'Independence', '3.jpg', 'The fate of humanity hangs in the balance as the U.S. President and citizens decide if these aliens are to be trusted ...or feared.', 'Yes'),
(4, 'Central Intelligence', '4.jpg', 'Bullied as a teen for being overweight, Bob Stone (Dwayne Johnson) shows up to his high school reunion looking fit and muscular. Claiming to be on a top-secret ...', 'Yes'),
(5, 'Ice Age', '6.jpg', 'In the film''s epilogue, Scrat keeps struggling to control the alien ship until it crashes on Mars, destroying all life on the planet.', 'Yes'),
(6, 'X - Man', '7.jpg', 'In 1977, paranormal investigators Ed (Patrick Wilson) and Lorraine Warren come out of a self-imposed sabbatical to travel to Enfield, a borough in north ...', 'Yes'),
(7, 'Tamas123', 'tamas-banner.jpg', 'This TV miniseries ("Darkness" in English) became famous in India in the mid/late 80s for its realistic depiction of the partition of the Indian subcontinent.', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `base_setup`
--

CREATE TABLE `base_setup` (
  `id` int(11) NOT NULL,
  `header_contact_no` varchar(20) NOT NULL,
  `header_email` varchar(99) NOT NULL,
  `social_link_fb` varchar(200) NOT NULL,
  `social_link_twitter` varchar(200) NOT NULL,
  `social_link_vine` varchar(200) NOT NULL,
  `social_link_linkedin` varchar(200) NOT NULL,
  `social_link_dribbble` varchar(200) NOT NULL,
  `social_link_insta` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `base_setup`
--

INSERT INTO `base_setup` (`id`, `header_contact_no`, `header_email`, `social_link_fb`, `social_link_twitter`, `social_link_vine`, `social_link_linkedin`, `social_link_dribbble`, `social_link_insta`) VALUES
(1, '+1 234 567 7890', 'queries@rentalwebsite.com', 'https://www.facebook.com/', 'https://twitter.com/', 'https://vine.co/', 'https://www.linkedin.com/', 'https://dribbble.com/', 'https://www.instagram.com/');

-- --------------------------------------------------------

--
-- Table structure for table `country_info`
--

CREATE TABLE `country_info` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country_info`
--

INSERT INTO `country_info` (`country_id`, `country_name`) VALUES
(1, 'Argentina'),
(2, 'Austria'),
(3, 'Bolivia'),
(4, 'Bulgaria'),
(5, 'China'),
(6, 'Denmark'),
(7, 'Finland'),
(8, 'Georgia'),
(9, 'Hong Kong'),
(10, 'India'),
(11, 'Israel'),
(12, 'Japan'),
(13, 'Lebanon'),
(14, 'Malaysia'),
(15, 'Monaco'),
(16, 'New Zealand'),
(17, 'Palau'),
(18, 'Philippines'),
(19, 'Puerto Rico'),
(20, 'Russia'),
(21, 'Singapore'),
(22, 'South Africa'),
(23, 'Sri Lanka'),
(24, 'Taiwan'),
(25, 'Tunisia'),
(26, 'Ukraine'),
(27, 'Vietnam'),
(28, 'Zambia'),
(29, 'Aruba'),
(30, 'Bahamas'),
(31, 'Botswana'),
(32, 'Canada'),
(33, 'Congo'),
(34, 'Federal Republic of Yugoslavia'),
(35, 'France'),
(36, 'Germany'),
(37, 'Hungary'),
(38, 'Indonesia'),
(39, 'Italy'),
(40, 'Jordan'),
(41, 'Lithuania'),
(42, 'Malta'),
(43, 'Morocco'),
(44, 'Nigeria'),
(45, 'Panama'),
(46, 'Poland'),
(47, 'Qatar'),
(48, 'Saudi Arabia'),
(49, 'Slovakia'),
(50, 'South Korea'),
(51, 'Sweden'),
(52, 'Tanzania'),
(53, 'Turkey'),
(54, 'United Arab Emirates'),
(55, 'West Germany'),
(56, 'Australia'),
(57, 'Belgium'),
(58, 'Brazil'),
(59, 'Chile'),
(60, 'Czech Republic'),
(61, 'Fiji'),
(62, 'French Polynesia'),
(63, 'Greece'),
(64, 'Iceland'),
(65, 'Ireland'),
(66, 'Jamaica'),
(67, 'Kenya'),
(68, 'Luxembourg'),
(69, 'Mexico'),
(70, 'Netherlands'),
(71, 'Norway'),
(72, 'Peru'),
(73, 'Portugal'),
(74, 'Romania'),
(75, 'Serbia'),
(76, 'Slovenia'),
(77, 'Spain'),
(78, 'Switzerland'),
(79, 'Thailand'),
(80, 'UK'),
(81, 'USA'),
(82, 'Yugoslavia');

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

CREATE TABLE `general_settings` (
  `id` int(5) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `header_top_section1_icon` varchar(20) NOT NULL,
  `header_top_section1_content` varchar(30) NOT NULL,
  `header_top_section2_icon` varchar(20) NOT NULL,
  `header_top_section2_content` varchar(30) NOT NULL,
  `footer_col1_title_image` varchar(255) NOT NULL,
  `footer_col1_content` text NOT NULL,
  `footer_col2_title` varchar(255) NOT NULL,
  `footer_col2_content` text NOT NULL,
  `footer_col3_title` varchar(255) NOT NULL,
  `footer_col3_content` text NOT NULL,
  `footer_col4_title` varchar(255) NOT NULL,
  `footer_col4_content` text NOT NULL,
  `footer_copyright_left` text NOT NULL,
  `footer_copyright_right` text NOT NULL,
  `movie_page_sidebar_title` varchar(99) NOT NULL,
  `movie_page_sidebar_count` int(2) NOT NULL,
  `tv_show_page_sidebar_title` varchar(99) NOT NULL,
  `tv_show_page_sidebar_count` int(2) NOT NULL,
  `front_end_color` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `logo`, `favicon`, `header_top_section1_icon`, `header_top_section1_content`, `header_top_section2_icon`, `header_top_section2_content`, `footer_col1_title_image`, `footer_col1_content`, `footer_col2_title`, `footer_col2_content`, `footer_col3_title`, `footer_col3_content`, `footer_col4_title`, `footer_col4_content`, `footer_copyright_left`, `footer_copyright_right`, `movie_page_sidebar_title`, `movie_page_sidebar_count`, `tv_show_page_sidebar_title`, `tv_show_page_sidebar_count`, `front_end_color`) VALUES
(1, 'logo.png', 'favicon.png', 'fa fa-envelope', 'contact@yourwebsite.com', 'fa fa-phone', '+91-98765 43210', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Information', '<li><a href="#">FAQ</a></li>\r\n<li><a href="#">DMCA</a></li>\r\n<li><a href="#">Action</a></li>\r\n<li><a href="#">Adventure</a></li>\r\n<li><a href="#">Comedy</a></li>\r\n<li><a href="#">Icons</a></li>\r\n<li><a href="#">Contact Us</a></li>', 'Movies', '<li><a href="#">Movies</a></li>\r\n<li><a href="#">FAQ</a></li>\r\n<li><a href="#">Action</a></li>\r\n<li><a href="#">Adventure</a></li>\r\n<li><a href="#">Comedy</a></li>\r\n<li><a href="#">Icons</a></li>\r\n<li><a href="#">Contact Us</a></li>', 'TV Shows', '<li><a href="#">Movies</a></li>\r\n<li><a href="#">FAQ</a></li>\r\n<li><a href="#">Action</a></li>\r\n<li><a href="#">Adventure</a></li>\r\n<li><a href="#">Comedy</a></li>\r\n<li><a href="#">Icons</a></li>\r\n<li><a href="#">Contact Us</a></li>', 'Your Website Name.', 'Support by <a href=''#''>Company Name.</a>', 'Movies You May Like', 3, 'TV Shows You May Like', 4, '#ff0000');

-- --------------------------------------------------------

--
-- Table structure for table `genre_info`
--

CREATE TABLE `genre_info` (
  `genre_id` int(11) NOT NULL,
  `genre_type` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genre_info`
--

INSERT INTO `genre_info` (`genre_id`, `genre_type`, `slug`) VALUES
(1, 'Action', 'action'),
(2, 'Animation', 'animation'),
(3, 'Crime', 'crime'),
(4, 'Family', 'fantasy'),
(5, 'History', 'history'),
(6, 'Musical', 'musical'),
(7, 'Sci-Fi', 'sci-fi'),
(8, 'Thriller', 'thriller'),
(9, 'Adult', 'adult'),
(10, 'Biography', 'biography'),
(11, 'Documentary', 'documentary'),
(12, 'Fantasy', 'fantasy'),
(13, 'Horror', 'horror'),
(14, 'Mystery', 'mystery'),
(15, 'Short', 'short'),
(16, 'Western', 'western'),
(17, 'Adventure', 'adventure'),
(18, 'Comedy', 'comedy'),
(19, 'Drama', 'drama'),
(20, 'Film-Noir', 'film-noir'),
(21, 'Music', 'music'),
(22, 'Romance', 'romance'),
(23, 'Sports', 'sports'),
(24, 'War', 'war');

-- --------------------------------------------------------

--
-- Table structure for table `movies_info`
--

CREATE TABLE `movies_info` (
  `movie_id` int(11) NOT NULL,
  `movie_name` varchar(255) NOT NULL,
  `movie_original_name` varchar(255) NOT NULL,
  `movie_image` varchar(255) NOT NULL,
  `movie_description` longtext NOT NULL,
  `movie_stars` varchar(255) NOT NULL,
  `movie_director` varchar(255) NOT NULL,
  `movie_trailer` varchar(255) NOT NULL,
  `release_date` date NOT NULL,
  `release_year` year(4) NOT NULL,
  `movie_genre` varchar(99) NOT NULL,
  `movie_rating` varchar(99) NOT NULL,
  `movie_country` varchar(255) NOT NULL,
  `movie_runtime` varchar(99) NOT NULL,
  `is_featured` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `views_count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `movies_info`
--

INSERT INTO `movies_info` (`movie_id`, `movie_name`, `movie_original_name`, `movie_image`, `movie_description`, `movie_stars`, `movie_director`, `movie_trailer`, `release_date`, `release_year`, `movie_genre`, `movie_rating`, `movie_country`, `movie_runtime`, `is_featured`, `status`, `views_count`) VALUES
(1, 'The Jungle Book', 'The Jungle Book', 'jungle-book-poster.jpg', 'After a threat from the tiger Shere Khan forces him to flee the jungle, a man-cub named Mowgli embarks on a journey of self discovery with the help of panther Bagheera and free-spirited bear Baloo.', 'Neel Sethi, Bill Murray, Ben Kingsley', 'Jon Favreau', 'HcgJRQWxKnw', '2016-04-15', 2016, 'Adventure,Drama,Family,Fantasy', '7.4', 'UK,USA', '106 min', 'Yes', '', 36),
(2, 'Central Intelligence', 'Central Intelligence', 'central-intelligence-poster.jpg', 'After he reconnects with an awkward pal from high school through Facebook, a mild-mannered accountant is lured into the world of international espionage.', 'Dwayne Johnson, Kevin Hart, Jason Bateman, Aaron Paul, Amy Ryan, Ryan Hansen, Danielle Nicolet, Tim Griffin', 'Rawson Marshall Thurber', 'MxEw3elSJ8M', '2016-06-17', 2016, 'Action,Comedy,Crime', '6.3', 'USA,China', '107 min', 'Yes', '', 71),
(3, 'Jason Bourne', 'Jason Bourne', 'jason-bourne-poster.jpg', 'The CIA''s most dangerous former operative is drawn out of hiding to uncover more explosive truths about his past.', 'Matt Damon, Tommy Lee Jones, Alicia Vikander', 'Paul Greengras', 'LuK4AiiB1Vk', '2016-06-29', 2016, 'Action,Thriller', '6.6', 'USA,China', '123 min', 'Yes', '', 31),
(4, 'X-Men: Apocalypse', 'X-Men: Apocalypse', 'x-men-apocalypse-poster.jpg', 'After the re-emergence of the world''s first mutant, the world-destroyer Apocalypse, the X-Men must unite to defeat his extinction level plan.', 'James McAvoy, Michael Fassbender, Jennifer Lawrence', 'Bryan Singer', 'COvnHv42T-A', '2016-05-27', 2016, 'Action,Adventure,Sci-Fi', '7', 'USA', '144 min', 'Yes', '', 10),
(5, 'Ice Age: Collision Course', 'Ice Age: Collision Course', 'ice-age-collision-course-poster.jpg', 'Manny, Diego, and Sid join up with Buck to fend off a meteor strike that would destroy the world. ', 'Ray Romano, Denis Leary, John Leguizamo', 'Mike Thurmeier (as Michael Thurmeier),Galen T. Chu (co-director) (as Galen Tan Chu)', 'yPmm1JhygIo', '2016-07-22', 2016, 'Animation,Adventure,Comedy,Family,Fantasy,Sci-Fi', '5.7', 'USA', '94 min', 'Yes', '', 3),
(6, 'Dead 7', 'Dead 7', 'dead-7-poster.jpg', 'A post-apocalyptic Western that follows a group of gunslingers as they look to rid a small town of a zombie plague.', 'Nick Carter, Carrie Keagan, Joey Fatone', 'Danny Roew', 'FYHGujAx-xY', '2016-04-01', 2016, 'Comedy,Horror,Western', '3.1', 'USA', '89 min', 'Yes', '', 1),
(7, '24', '24', '24-poster.jpg', 'A scientist invents a time machine, which leads to a bitter battle between his evil twin brother and his son.', 'Suriya, Samantha Ruth Prabhu, Nithya Menon', 'Vikram K. Kumar', 'wqXE_es_I3M', '2016-05-06', 2016, 'Action,Sci-Fi,Thriller', '7.9', 'India', '164 min', 'Yes', '', 4),
(8, 'The Legend of Tarzan', 'The Legend of Tarzan', 'the-legend-of-tarzan-poster.jpg', 'Tarzan, having acclimated to life in London, is called back to his former home in the jungle to investigate the activities at a mining encampment.', 'Alexander Skarsg?rd, Rory J. Saper, Christian Stevens', 'David Yates', 'Aj7ty6sViiU', '2016-07-01', 2016, 'Action,Adventure,Drama,Fantasy,Romance', '6.3', 'UK,Canada,USA', '110 min', 'Yes', '', 12),
(9, 'Moana', 'Moana', 'moana-poster.jpg', 'In Ancient Polynesia, when a terrible curse incurred by the Demigod Maui reaches Moana''s island, she answers the Ocean''s call to seek out the Demigod to set things right.', 'Auli''i Cravalho, Dwayne Johnson, Rachel House', 'Ron Clements, John Musker', 'LKFuXETZUsI', '2016-11-23', 2016, 'Animation,Adventure,Comedy,Family,Fantasy,Musical', '7.6', 'USA', '107 min', 'Yes', '', 5),
(10, 'Dirty Grandpa', 'Dirty Grandpa', 'dirty-grandpa-poster.jpg', 'Right before his wedding, an uptight guy is tricked into driving his grandfather, a lecherous former Army Lieutenant Colonel, to Florida for Spring Break.', 'Robert De Niro, Zac Efron, Zoey Deutch', 'Dan Mazer', 'cg8plmKLqz8', '2016-01-22', 2016, 'Comedy', '5.9', 'USA', '102 min', 'Yes', '', 1),
(11, 'Ride Along 2', 'Ride Along 2', 'ride-along-2-poster.jpg', 'As his wedding day approaches, Ben heads to Miami with his soon-to-be brother-in-law James to bring down a drug dealer who''s supplying the dealers of Atlanta with product.', 'Ice Cube, Kevin Hart, Tika Sumpter', 'Tim Story', 'iWfmmwdCHTg', '2016-01-15', 2016, 'Action,Comedy', '5.9', 'USA', '102 min', 'Yes', '', 7),
(12, 'Don''t Think Twice', 'Don''t Think Twice', 'dont-think-twice-poster.jpg', 'When a member of a popular New York City improv troupe gets a huge break, the rest of the group - all best friends - start to realize that not everyone is going to make it after all.', 'Keegan-Michael Key, Gillian Jacobs, Mike Birbiglia', 'Mike Birbiglia', 'iPwIBBuJps0', '2016-07-22', 2016, 'Comedy,Drama', '6.8', 'USA', '92 min', 'Yes', '', 4),
(13, 'Bad Moms', 'Bad Moms', 'bad-moms-poster.jpg', 'When three overworked and under-appreciated moms are pushed beyond their limits, they ditch their conventional responsibilities for a jolt of long overdue freedom, fun, and comedic self-indulgence.', 'Mila Kunis, Kathryn Hahn, Kristen Bell', 'Jon Lucas, Scott Moore', 'iKCw-kqo3cs', '2016-07-29', 2016, 'Comedy', '6.2', 'USA', '100 min', 'Yes', '', 7),
(14, 'Ben-Hur', 'Ben-Hur', 'ben-hur-poster.jpg', 'Judah Ben-Hur, a prince falsely accused of treason by his adopted brother, an officer in the Roman army, returns to his homeland after years at sea to seek revenge, but finds redemption.', 'Jack Huston, Toby Kebbell, Rodrigo Santoro', 'Timur Bekmambetov', 'gLJdzky63BA', '2016-08-19', 2016, 'Action,Adventure,Drama,History', '5.7', 'USA', '123 min', 'Yes', '', 0),
(15, 'War Dogs', 'War Dogs', 'war-dogs-poster.jpg', 'Loosely based on the true story of two young men, David Packouz and Efraim Diveroli, who won a three hundred million dollar contract from the Pentagon to arm America''s allies in Afghanistan.', 'Jonah Hill, Miles Teller, Steve Lantz', 'Todd Phillips', 'Rwh9c_E3dJk', '2016-08-19', 2016, 'Comedy,Crime,Drama,War', '7.1', 'USA,Cambodia', '114 min', 'Yes', '', 1),
(16, 'Mechanic: Resurrection', 'Mechanic: Resurrection', 'mechanic-resurrection-poster.jpg', 'Bishop thought he had put his murderous past behind him, until his most formidable foe kidnaps the love of his life. Now he is forced to complete three impossible assassinations, and do what he does best: make them look like accidents.', 'Jason Statham, Jessica Alba, Tommy Lee Jones', 'Dennis Gansel', 'QF903RaKLvs', '2016-08-26', 2016, 'Action,Adventure,Crime,Thriller', '5.7', 'France,USA', '98 min', 'Yes', '', 12),
(17, 'The Light Between Oceans', 'The Light Between Oceans', 'the-light-between-oceans-poster.jpg', 'A lighthouse keeper and his wife living off the coast of Western Australia raise a baby they rescue from a drifting rowing boat.', 'Michael Fassbender, Alicia Vikander, Rachel Weisz', 'Derek Cianfrance', 'lk7yw00a4fs', '2016-09-02', 2016, 'Drama,Romance', '7.2', 'UK,New Zealand,USA', ' 133 min', 'Yes', '', 2),
(18, 'The BFG', 'The BFG', 'the-bfg-poster.jpg', 'An orphan little girl befriends a benevolent giant who takes her to Giant Country, where they attempt to stop the man-eating giants that are invading the human world.', 'Mark Rylance, Ruby Barnhill, Penelope Wilton', 'Steven Spielberg', 'GZ0Bey4YUGI', '2016-07-01', 2016, 'Adventure,Family,Fantasy', '6.4', 'USA,India', '117 min', 'Yes', '', 2),
(19, 'Greater', 'Greater', 'greater-poster.jpg', 'The story of Brandon Burlsworth, possibly the greatest walk-on in the history of college football. ', 'Neal McDonough, Leslie Easterbrook, Christopher Severio', 'David Hunt', 'v0Ow6lhvPNk', '2016-08-26', 2016, 'Biography,Family,Sport', '7.2', 'USA', '130 min', 'Yes', '', 0),
(20, 'Citizen Soldier', 'Citizen Soldier', 'citizen-soldier-poster.jpg', 'CITIZEN SOLDIER is a dramatic feature film, told from the point of view of a group of Soldiers in the Oklahoma Army National Guard''s 45th Infantry Brigade Combat Team, known since World War II as the "Thunderbirds." Set in one of the most dangerous parts of Afghanistan at the height of the surge, it is a heart-pounding, heartfelt grunts'' eye-view of the war. A modern day Band of Brothers, Citizen Soldier tells the true story of a group of Soldiers and their life-changing tour of duty in Afghanistan, offering an excruciatingly personal look into modern warfare, brotherhood, and patriotism. Using real footage from multiple cameras, including helmet cams, these Citizen Soldiers give the audience an intimate view into the chaos and horrors of combat and, in the process, display their bravery and valor under the most hellish of conditions.', 'Jordan Alex, James Tyler Brown, Martin Byrne', 'David Salzberg, Christian Tureaud', '#NAME?', '2016-08-05', 2016, 'Documentary,Action', '5.7', 'USA', '105 min', 'Yes', '', 1),
(21, 'God''s Compass', 'God''s Compass', 'gods-compass-poster.jpg', 'On the night Suzanne Waters celebrates her retirement, she is faced with a series of crisis she could not have imagined.', 'Karen Abercrombie, T.C. Stallings, Jazelle Foster', 'Stephan Schultze', 'qLtD4orE2r4', '2016-05-03', 2016, 'Drama', '7.5', 'USA', '99 min', 'Yes', '', 3),
(22, 'Beyond the Edge', 'ISRA 88', 'isra-88-poster.jpg', 'A scientist and a pilot volunteer for a high profile mission to reach the end of the universe. After 13 years, the ship crashes through the end of the universe and into the unknown.', 'Casper Van Dien, Sean Maher, Adrienne Barbeau', 'Thomas Zellen', 'u4prn8EXG3k', '2017-02-07', 2017, 'Adventure,Mystery,Sci-Fi,Thriller', '3.8', 'USA', '119 min', 'Yes', '', 18),
(23, 'Tik Tik Tik', 'Tik Tik Tik', 'tik-tik-tik-poster.jpg', 'After discovering that an asteroid the size of a city is going to impact Earth in less than a month, India recruits a misfit team to save the country.', 'Nivetha Pethuraj, Jayam Ravi, Aarav', 'Shakti Soundar Rajan', '6ug9zvC9pJM', '2018-06-22', 2018, 'Action,Adventure,Sci-Fi', '7.6', 'India', '130 min', 'Yes', '', 1),
(24, 'Thamizh Padam 2', 'Thamizh Padam 2', 'thamizh-padam-2-poster.jpg', 'A stringent cop tries all the possible ways to nab a dreaded don who poses a big threat to the society.', 'Shiva, Iswarya Menon, Disha Pandey', 'C.S. Amudhan', 'vFWlCsjWFMw', '2018-07-12', 2018, 'Comedy', '7.7', 'India', '143 min', 'Yes', '', 11),
(25, 'Tangled', 'Tangled', 'tangled-poster.jpg', 'The magically long-haired Rapunzel has spent her entire life in a tower, but now that a runaway thief has stumbled upon her, she is about to discover the world for the first time, and who she really is.', 'Mandy Moore, Zachary Levi, Donna Murphy', 'Nathan Greno, Byron Howard', '2f516ZLyC6U', '2010-11-24', 2010, 'Animation,Adventure,Comedy,Family,Fantasy,Musical,Romance', '7.8', 'USA', '100 min', 'Yes', '', 31),
(26, 'Ratatouille', 'Ratatouille', 'ratatouille-poster.jpg', 'A rat who can cook makes an unusual alliance with a young kitchen worker at a famous restaurant.', 'Brad Garrett, Lou Romano, Patton Oswalt', 'Brad Bird, Jan Pinkava (co-director)', 'e8GBfNo3IHY', '2007-06-29', 2007, 'Animation,Adventure,Comedy,Drama,Family,Fantasy', '8', 'USA', '111 min', 'Yes', '', 20),
(27, 'Ocean''s Eleven', 'Ocean''s Eleven', 'oceans-eleven-poster.jpg', 'Danny Ocean and his eleven accomplices plan to rob three Las Vegas casinos simultaneously.', 'George Clooney, Brad Pitt, Julia Roberts', 'Steven Soderbergh', 'n3epi9hPbqQ', '2001-12-07', 2001, 'Crime,Thriller', '7.8', 'USA', '116 min', 'Yes', '', 6),
(28, 'On the Road', 'On the Road', 'on-the-road-poster.jpg', 'Young writer Sal Paradise has his life shaken by the arrival of free-spirited Dean Moriarty and his girl, Marylou. As they travel across the country, they encounter a mix of people who each impact their journey indelibly.', '', '', 'su75_mcryO4', '2012-05-23', 2012, 'Adventure, Drama, Romance', '6.1', 'France,USA,UK,Brazil,Canada,Argentina', '137 min', 'Yes', '', 7),
(29, 'The Three Musketeers', 'The Three Musketeers', 'the-three-musketeers-poster.jpg', 'The three best of the disbanded Musketeers - Athos, Porthos, and Aramis - join a young hotheaded would-be-Musketeer, D''Artagnan, to stop the Cardinal Richelieu''s evil plot: to form an alliance with enemy England by way of the mysterious Milady. Rochefort, the Cardinal''s right-hand man, announces the official disbanding of the King''s Musketeers. Three, however, refuse to throw down their swords - Athos the fighter and drinker, Porthos the pirate and lover, and Aramis the priest and poet. Arriving in Paris to join the Musketeers, D''Artagnan uncovers the Cardinal''s plans, and the four set out on a mission to protect King and Country.', '', '', 'a22U0KgXxjQ', '1993-11-12', 1993, 'Action, Family, Adventure, Comedy, Romance', '6.4', 'Austria, UK, USA', '105 min', 'Yes', '', 5),
(30, 'Blackthorn', 'Blackthorn', 'blackthorn-poster.jpg', 'In Bolivia, Butch Cassidy (now calling himself James Blackthorn) pines for one last sight of home, an adventure that aligns him with a young robber and makes the duo a target for gangs and lawmen alike.', '', '', 'uel9ShY_zbM', '2011-07-01', 2011, 'Action, Western, Adventure', '6.6', 'Bolivia, France, Spain, UK', '102 min', 'Yes', '', 4),
(31, 'May in the Summer', 'May in the Summer', 'may-in-the-summer-poster.jpg', 'High off the success of her first book and planning to marry ZIAD, her sensible, stable and studious fiance, MAY BRENNAN has it all. At least that&#39s what she&#39d like people to believe. Reunited with her family in Amman, she&#39s thrust back into the chaos of her former existence. Her headstrong mother NADINE, a born-again Christian disapproves of her Muslim fiance so thoroughly she plans to boycott the wedding. Her younger sisters DALIA and YASMINE behave like her children. And her estranged father EDWARD is suddenly and suspiciously interested in making amends. As her wedding day looms, May finds herself more and more confronted by the trauma of her parents divorce. And soon, her once carefully structured life spins hopelessly out of control.', '', '', 'HcgJRQWxKnw', '2013-08-21', 2013, 'Comedy,Drama', '5.7', 'Jordan,Qatar,USA', '99 min', 'Yes', '', 2),
(32, 'Avanti popolo', 'Avanti popolo', 'avanti-popolo-poster.jpg', 'June 11th 1967, the Six-Day War is over and the cease fire has just begun. We follow the journey of Gassan and Haled, two Egyptian soldiers whose only wish is to make their way through the Sinai desert and safely reach the Suez Canal. Thus begins a comical, almost surrealistic saga during which they meet various groups of people across the desert, including Israeli soldiers on patrol and a pushy news reporter.', '', '', '#NAME?', '1986-11-26', 1986, 'Drama,War', '7.7', 'Israel', '84 min', 'Yes', '', 3),
(33, 'Warawara', 'Warawara', 'warawara-poster.jpg', 'The story of a an Inca Princess who falls in love with a Captain of the Spanish army. ', '', '', '6dHnRC5mcBA', '1930-01-01', 1930, 'Drama', '6.4', 'Bolivia', '69 min', 'Yes', '', 0),
(34, 'Come Back, Sebastiana', 'Vuelve Sebastiana', 'vuelve-sebastiana-poster.jpg', 'The story of a poor girl who leaves her starving family and sheep for a more prosperous village. Her grandfather finds her and tries to convince her to return to her home. ', '', '', 'PZ__df6ilwk', '1953-01-01', 1953, 'Short', '6.7', 'Bolivia', '28 min', 'Yes', '', 2),
(35, 'The Monkey King', 'Xi you ji: Da nao tian gong', 'the-monkey-king-poster.jpg', 'A monkey born from heavenly stone acquires supernatural powers and must battle the armies of both gods and demons to find his place in the heavens.', '', '', 'TNjH_Todn-U', '2014-01-30', 2014, 'Action,Adventure,Family', '4.8', 'China,Hong Kong', '119 min', 'Yes', '', 0),
(36, 'Dragon Blade', 'Tian jiang xiong shi', 'dragon-blade-poster.jpg', 'When corrupt Roman leader Tiberius arrives with a giant army to claim the Silk Road, Huo An teams up his army with an elite Legion of defected Roman soldiers led by General Lucius to protect his country and his new friends.', '', '', '6QIoL-Cnh1A', '2015-09-04', 2015, 'Action,Adventure,Drama,History', '6', 'China,Hong Kong', '127 min', 'Yes', '', 2),
(37, 'House of Flying Daggers', 'Shi mian mai fu', 'house-of-flying-daggers-poster.jpg', 'A romantic police captain breaks a beautiful member of a rebel group out of prison to help her rejoin her fellows, but things are not what they seem.', '', '', 'zLkedDMb8vI', '2004-07-15', 2004, 'Action,Adventure,Drama,Romance', '7.6', 'China,Hong Kong', '119 min', 'Yes', '', 1),
(38, 'Voevoda', 'Voevoda', 'voevoda-poster.jpg', 'A mother, forced to abandon her child and home, leads a rebellious band and inflicts her own justice in the brutal men''s world of 19th century.', '', '', 'ySlxiS9oils', '2017-01-13', 2017, 'Action,Biography,Drama,History,War', '6.6', 'Bulgaria,Croatia', '126 min', 'Yes', '', 1),
(39, 'Acts Of Vengeance', 'Acts Of Vengeance', 'acts-of-vengeance-poster.jpg', 'A fast-talking lawyer transforms his body and takes a vow of silence, not to be broken until he finds out who killed his wife and daughter and has his revenge.', '', '', 'f4Lrm7w6O4M', '2017-10-27', 2017, 'Action,Drama,Thriller', '5.7', 'Bulgaria', '87 min', 'Yes', '', 1),
(40, 'The Hunt', 'Jagten', 'jagten-poster.jpg', 'A teacher lives a lonely life, all the while struggling over his son''s custody. His life slowly gets better as he finds love and receives good news from his son, but his new luck is about to be brutally shattered by an innocent little lie.', '', '', 'ieLIOBkMgAQ', '2013-01-10', 2013, 'Drama', '8.3', 'Denmark,Sweden', '115 min', 'Yes', '', 0),
(41, 'Melancholia', 'Melancholia', 'melancholia-poster.jpg', 'Two sisters find their already strained relationship challenged as a mysterious new planet threatens to collide with Earth.', '', '', '4kcnBvSLI98', '2011-05-26', 2011, 'Drama,Sci-Fi', '7.1', 'Denmark,Sweden,France,Germany', '135 min', 'Yes', '', 2),
(42, 'Tom of Finland', 'Tom of Finland', 'tom-of-finland-poster.jpg', 'Award-winning filmmaker Dome Karukoski brings to screen the life and work of artist Touko Valio Laaksonen (aka Tom of Finland), one of the most influential and celebrated figures of twentieth century gay culture.', '', '', 'gxPRRzZkpus', '2017-02-24', 2017, 'Biography,Drama', '6.8', 'Finland,Sweden,Denmark,Germany,Iceland,USA', '115 min', 'Yes', '', 1),
(43, 'Iron Sky', 'Iron Sky', 'iron-sky-poster.jpg', 'The Nazis set up a secret base on the dark side of the moon in 1945 where they hide out and plan to return to power in 2018.', '', '', 'Py_IndUbcxc', '2012-04-04', 2012, 'Action,Sci-Fi,Comedy', '5.9', 'Finland,Germany,Australia', '93 min', 'Yes', '', 0),
(44, '247?F', '247?F', '247deg-f-poster.jpg', 'Four friends travel to a lakeside cabin for a carefree weekend, the fun turns into a nightmare when 3 of them end up locked in a hot sauna. Every minute counts and every degree matters as they fight for their lives in the heat up to 247?F.', '', '', '2dohHZf08Wk', '2011-08-01', 2011, 'Horror,Thriller', '5', 'USA,Georgia', '88 min', 'Yes', '', 3),
(45, 'Howl''s Moving Castle', 'Hauru no ugoku shiro', 'howls-moving-castle-poster.jpg', 'When an unconfident young woman is cursed with an old body by a spiteful witch, her only chance of breaking the spell lies with a self-indulgent yet insecure young wizard and his companions in his legged, walking castle.', '', '', 'Q6BBw7N7rO0', '2005-06-17', 2005, ' Animation,Adventure,Family,Fantasy', '8.2', 'Japan', '119 min', 'Yes', '', 2),
(46, 'Mamma Mia! Here We Go Again', 'Mamma Mia! Here We Go Again', 'mamma-mia-here-we-go-again.jpg', 'Five years after the events of Mamma Mia! (2008), Sophie learns about her mother''s past while pregnant herself.', 'Lily James,Amanda Seyfried,Meryl Streep', 'Ol Parker', 'wLXDrfgnBfI', '2018-07-20', 2018, 'Comedy,Musical', '7.3', 'UK,USA', '114 min', 'Yes', '', 12),
(47, 'Following', 'Following', 'following-poster.jpg', 'A young writer who follows strangers for material meets a thief who takes him under his wing.', 'Jeremy Theobald, Alex Haw, Lucy Russell', 'Christopher Nolan', 'RHRnYeZL5Pc', '1999-04-04', 1999, 'Crime,Mystery,Thriller', '7.6', 'UK', ' 69 min', 'Yes', '', 9),
(48, 'Memento', 'Memento', 'momento-poster.jpg', 'A man with short-term memory loss attempts to track down his wife''s murderer.', 'Guy Pearce, Carrie-Anne Moss, Joe Pantoliano', 'Christopher Nolan', 'UFWAE1CffbY', '2001-05-25', 2001, 'Mystery,Thriller', '8.5', 'USA', '113 min', 'Yes', '', 0),
(49, 'Insomnia', 'Insomnia', 'insomnia-poster.jpg', 'Two Los Angeles homicide detectives are dispatched to a northern town where the sun doesn''t set to investigate the methodical murder of a local teen.', 'Al Pacino, Robin Williams, Hilary Swank', 'Christopher Nolan', 'V8Vu7Zre7h8', '2002-05-24', 2002, 'Drama,Mystery,Thriller', '7.2', 'USA', '118 min', 'Yes', '', 1),
(50, 'Batman Begins', 'Batman Begins', 'batman-begins-poster.jpg', 'After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from the corruption that Scarecrow and the League of Shadows have cast upon it.', 'Christian Bale, Michael Caine, Ken Watanabe', 'Christopher Nolan', 'eJGyZB685Gs', '2005-06-15', 2005, 'Action,Adventure', '8.3', 'USA,UK', '140 min', 'Yes', '', 0),
(51, 'The Prestige', 'The Prestige', 'the-prestige-poster.jpg', 'After a tragic accident, two stage magicians engage in a battle to create the ultimate illusion while sacrificing everything they have to outwit each other.', 'Christian Bale, Hugh Jackman, Scarlett Johansson', 'Christopher Nolan', '8Wpm_YhlkNQ', '2006-10-20', 2006, 'Drama,Mystery,Sci-Fi,Thriller', '8.5', 'USA,UK', '130 min', 'Yes', '', 1),
(52, 'The Dark Knight', 'The Dark Knight', 'the-dark-knight-poster.jpg', 'When the menace known as the Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. The Dark Knight must accept one of the greatest psychological and physical tests of his ability to fight injustice.', 'Christian Bale,Heath Ledger,Aaron Eckhart', 'Christopher Nolan', '5y2szViJlaY', '2008-07-18', 2016, 'Action,Crime,Drama,Thriller', '9', 'USA,UK', '152 min', 'Yes', '', 0),
(53, 'Inception', 'Inception', 'inception-poster.jpg', 'A thief, who steals corporate secrets through the use of dream-sharing technology, is given the inverse task of planting an idea into the mind of a CEO.', 'Leonardo DiCaprio, Joseph Gordon-Levitt, Ellen Page', 'Christopher Nolan', 'YoHD9XEInc0', '2010-07-16', 2010, 'Action,Adventure,Sci-Fi,Thriller', '8.8', 'USA,UK', '148 min', 'Yes', '', 1),
(54, 'The Dark Knight Rises', 'The Dark Knight Rises', 'the-dark-knight-rises-poster.jpg', 'Eight years after the Joker''s reign of anarchy, Batman, with the help of the enigmatic Catwoman, is forced from his exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.', 'Christian Bale,Tom Hardy,Anne Hathaway', 'Christopher Nolan', 'g8evyE9TuYk', '2012-07-20', 2012, 'Action,Thriller', '8.4', 'USA,UK', '164 min', 'Yes', '', 0),
(55, 'Interstellar', 'Interstellar', 'interstellar-poster.jpg', 'A team of explorers travel through a wormhole in space in an attempt to ensure humanity''s survival.', 'Matthew McConaughey,Anne Hathaway,Jessica Chastain', 'Christopher Nolan', 'ZA7DuE8k6BY', '2014-11-07', 2014, 'Adventure,Drama,Sci-Fi', '8.6', 'USA,UK', '169 min', 'Yes', '', 0),
(56, 'Dunkirk', 'Dunkirk', 'dunkirk-poster.jpg', 'Allied soldiers from Belgium, the British Empire and France are surrounded by the German Army, and evacuated during a fierce battle in World War II.', 'Fionn Whitehead,Barry Keoghan,Mark Rylance', 'Christopher Nolan', 'tk9yUcmYer0', '2017-07-21', 2017, 'Action,Drama,History,Thriller,War', '8', 'UK,Netherlands,France,USA', '106 min', 'Yes', '', 2),
(57, 'The Sugarland Express', 'The Sugarland Express', 'the-sugarland-express-poster.jpg', 'A woman attempts to reunite her family by helping her husband escape prison and together kidnapping their son. But things don''t go as planned when they are forced to take a police hostage on the road.', 'Goldie Hawn,Ben Johnson,Michael Sacks', 'Steven Spielberg', 'YeCPIkiPHQw', '1974-05-29', 1974, 'Crime,Drama', '6.8', 'USA', '110 min', 'Yes', '', 0),
(58, 'Close Encounters of the Third Kind', 'Close Encounters of the Third Kind', 'close-encounters-of-the-third-kind.jpg', 'After an accidental encounter with otherworldly vessels, an ordinary man follows a series of psychic clues to the first scheduled meeting between representatives of Earth and visitors from the cosmos.', 'Richard Dreyfuss,Fran?ois Truffaut,Teri Garr', 'Steven Spielberg', 'Fxp32VHaYdE', '1977-12-14', 1977, 'Drama,Sci-Fi', '7.7', 'USA', '135 min', 'Yes', '', 0),
(59, '1941', '1941', '1941-poster.jpg', 'Hysterical Californians prepare for a Japanese invasion in the days after Pearl Harbor.', 'John Belushi,Dan Aykroyd,Treat Williams', 'Steven Spielberg', 'vJK5_KAyfBM', '1979-12-14', 1979, 'Action,Comedy,War', '5.8', 'USA', '118 min', 'Yes', '', 0),
(60, 'Thalapathi', 'Thalapathi', 'thalapathi-poster.jpg', 'An orphan named Surya raised in a slum befriends a good crime boss named Devaraj and works for him. Their existence is threatened when a new honest district collector arrives.', 'Rajinikanth,Mammootty,Shobana', 'Mani Ratnam', 'pC5yWXqb8Ys', '1991-11-25', 1991, 'Action,Crime,Drama', '8.6', 'India', '157 min', 'Yes', '', 3),
(61, 'Mouna Ragam', 'Mouna Ragam', 'mouna-ragam-poster.jpg', 'Divya, a woman grieving over the death of her lover, is convinced into an arranged marriage with Chandra Kumar. Over a year the couple attempt to adjust to each other''s presence and live with each other.', 'Karthik,Mohan,V.K. Ramasami', 'Mani Ratnam', 'RFcDcdOmQos', '1986-08-15', 1986, 'Drama,Romance', '8.6', 'India', '146 min', 'Yes', '', 2),
(62, 'Moondram Pirai', 'Moondram Pirai', 'moondram-pirai-poster.jpg', 'A young woman regresses to childhood after suffering a head injury in a car crash. Lost, she ends up trapped at a brothel before being rescued by a lonely school teacher who falls in love with her.', 'Kamal Haasan,Sridevi,Poornam Vishwanathan', 'Balu Mahendra', 'fYripvHdOcc', '1982-02-19', 1982, 'Drama,Romance', '8.6', 'India', '143 min', 'Yes', '', 0),
(63, 'Thillu Mullu', 'Thillu Mullu', 'thillu-mullu-poster.jpg', 'Chandran , a young man fools his boss and attempts to woo his boss'' daughter.', 'Rajinikanth,Madhavi,Thengai Srinivasan', 'K. Balachander', '4d2OwVImCTI', '1981-05-01', 1981, 'Comedy', '8.6', 'India', '136 min', 'Yes', '', 1),
(64, 'Aaranya Kaandam', 'Aaranya Kaandam', 'aaranya-kaandam-poster.jpg', 'A mob boss must deal with a disgruntled mistress and a vanishing bag of cocaine.', 'Sampath Raj,Jackie Shroff,Ravi Krishna', 'Thiagarajan Kumararaja', 'qBJ_UpyQw_s', '2011-06-10', 2017, 'Action,Comedy,Crime', '8.6', 'India', '153 min', 'Yes', '', 1),
(65, 'Joker', 'Joker', 'joker-poster.jpg', 'Mannar (protagonist), a villager who declares himself to be the Indian President, protests the absurdities of the government.', 'Guru Somasundaram,Ramya Pandiyan,Gayathri', 'Raju Murugan', '4xr59w5r6Hw', '2016-08-12', 2016, 'Comedy,Drama', '8.6', 'India', '130 min', 'Yes', '', 0),
(66, 'Kuruthipunal', 'Kuruthipunal', 'kuruthipunal-poster.jpg', 'Two brave police officers infiltrate a terror group in order to bring it down.', 'Kamal Haasan,Arjun,Nassar', 'P.C. Sreeram', 'U3AMytkiAuk', '1995-10-23', 1995, 'Action,Drama,Thriller', '8.6', 'India', '143 min', 'Yes', '', 1),
(67, 'Iruvar', 'Iruvar', 'iruvar-poster.jpg', 'The real-life rivalry between M.G. Ramachandran and Karunanidhi is given the Mani Ratnam treatment.', 'Mohanlal,Prakash Raj,Aishwarya Rai Bachchan', 'Mani Ratnam', 'ChPknzchAOs', '1997-01-14', 1997, 'Biography,Drama', '8.6', 'India', '140 min', 'Yes', '', 0),
(68, 'Mahanadi', 'Mahanadi', 'mahanadi-poster.jpg', 'A man tries to get his family back in shape after a stint in jail.', 'Kamal Haasan,Sukanya,Poornam Vishwanathan', 'Santhana Bharathi', 'GSMuVPh_cXw', '1993-01-14', 2007, 'Crime,Drama', '8.6', 'India', '162 min', 'Yes', '', 3),
(69, 'Thani Oruvan', 'Thani Oruvan', 'thani-oruvan-poster.jpg', 'Siddharth Abimanyu, an influential scientist, is involved in various illegal medical practices. Mithran, an efficient IPS officer, decides to expose him.', 'Jayam Ravi,Arvind Swamy,Nayanthara', 'M. Raja', 'r5Lih8rKd6k', '2015-08-28', 2015, 'Action,Crime,Thriller', '8.5', 'India', '160 min', 'Yes', '', 3),
(70, 'Dhuruvangal Pathinaaru', 'Dhuruvangal Pathinaaru', 'dhuruvangal-pathinaaru-poster.jpg', 'A police officer retires after he loses his right leg in an accident while investigating a case. Five years later, he is forced to relive his past as he narrates the story to his friend''s son.', 'Rahman,Prakash Raghavan,Sharath Kumar', 'Karthick Naren', 'xpt2jfiL5GY', '2016-12-29', 2016, 'Action,Crime,Mystery,Thriller', '8.5', 'India', '105 min', 'Yes', '', 2),
(71, 'A Peck on the Cheek', 'Kannathil Muthamittal', 'kannathil-muthamittal-poster.jpg', 'A little girl is told by her parents that she is adopted. Determined to find her birth mother, she begs to be taken to Sri Lanka, where her mother works with a militant group of activists.', 'Madhavan,Simran,Keerthana Parthiepan', 'Mani Ratnam', 'QgCUNzuWH5A', '2002-02-14', 2002, 'Drama,War', '8.5', 'India', '123 min', 'Yes', '', 3),
(72, 'Michael Madana Kamarajan', 'Michael Madana Kamarajan', 'michael-madana-kamarajan-poster.jpg', 'A comedy of errors with identical quadruplets, separated at birth, coming together as adults.', 'Kamal Haasan,Kushboo,Urvashi', 'Sigitham Srinivasa Rao', 'M5t-TGHwpCg', '1990-01-01', 1990, 'Comedy', '8.5', 'India', '162 min', 'Yes', '', 3),
(73, 'Baasha', 'Baasha', 'baasha-poster.jpg', 'An auto driver, Manikam, desperately tries to hide his dark underworld side to keep his promise to his father.', 'Rajinikanth,Nagma,Raghuvaran', 'Suresh Krishna', 'r1f-Hlw51wo', '1995-01-15', 1995, 'Action,Crime,Drama', '8.3', 'India', '145 min', 'Yes', '', 3),
(74, 'Roja', 'Roja', 'roja-poster.jpg', 'A woman from a village in Tamil Nadu marries a sophisticated city dweller and moves with him to Kashmir, where all is rosy - until he gets kidnapped by militants.', 'Arvind Swamy,Madhoo,Pankaj Kapur', 'Mani Ratnam', 'LrcwQfOSxjg', '1992-08-15', 1992, 'Drama,Romance,Thriller', '8.2', 'India', '137 min', 'Yes', '', 1),
(75, 'Mission: Impossible - Fallout', 'Mission: Impossible - Fallout', 'mission-impossible-fallout-poster.jpg', 'Ethan Hunt and his IMF team, along with some familiar allies, race against time after a mission gone wrong.', 'Tom Cruise, Henry Cavill, Ving Rhames', 'Christopher McQuarrie', 'wb49-oV0F78', '2018-07-27', 2018, 'Action, Thriller, Adventure', '8.6', 'USA', '147 min', 'Yes', '', 46),
(76, 'The Mad Kings', 'Les rois du monde', 'the-mad-kings-poster.jpg', 'In Casteljaloux, a town in southwestern France, friendship, drunken nights, heat and tall tales set the pace of daily life. And here, men are Kings of the World. But when Jeannot gets out of prison, he has only one thing in mind: to win back Chantal, the love of his life, who moved in with the village butcher while he was serving time. A Greek tragedy that soon takes on the feel of a Western...\n', 'Sergi LÃ³pez, Eric Cantona, CÃ©line Sallette', 'Laurent Laffargue', 'Zr3Rl_m1W8Q', '2015-09-23', 2015, 'Comedy, Drama', '5.4', 'France', '100 min', 'Yes', '', 5),
(77, 'Fantastic Beasts: The Crimes of Grindelwald', 'Fantastic Beasts: The Crimes of Grindelwald', 'fantastic-beasts-the-crimes-of-grindelwald.jpg', 'The second installment of the Fantastic Beasts series set in J.K. Rowling''s Wizarding World featuring the adventures of magizoologist Newt Scamander. ', ' Eddie Redmayne, Katherine Waterston, Dan Fogler', 'David Yates', '5sEaYB4rLFQ', '2018-11-16', 2018, ' Adventure,Family,Fantasy', '2.2', 'UK,USA', '125 min', 'Yes', '', 9),
(78, 'Jaws', 'Jaws', 'jaws-poster.jpg', 'A local sheriff, a marine biologist and an old seafarer team up to hunt down a great white shark wrecking havoc in a beach resort.', 'Roy Scheider, Robert Shaw, Richard Dreyfuss', 'Steven Spielberg', '4pxkU9GVAoA', '1975-06-20', 1975, 'Adventure,Drama,Thriller', '8', 'USA', '124 min', 'Yes', '', 5),
(79, 'Thevar Magan', 'Thevar Magan', 'thevar-magan-poster.jpg', '<p>The urbane son of a village chieftain struggles between his personal aspirations and those of his family.</p>\r\n', 'Kamal Haasan,Shivaji Ganesan,Nassar', 'Bharathan', 'Yhu1RKuHqd4', '1992-10-25', 1992, 'Drama', '8.8', 'India', ' 145 min', 'Yes', '', 7),
(80, 'Joker', 'Joker', 'joker-poster.jpg', 'Joker Movie Desc', 'cast01,cast02,cast03', 'director name', 'YuZk098_V', '2019-10-07', 2019, '0', '9.0', '0', '125', 'Yes', '', 0),
(81, 'Shazam', 'Shazam', 'shazam-poster.jpg', 'Shazam Description', 'shazam01,shazam02', 'shazam director', 'YuTwzx08z', '2018-09-12', 2018, 'Action,Family,Comedy,', '7', 'USA', '129', 'Yes', '', 0),
(82, 'Aquaman', 'Aquaman', 'aquaman-poster.jpg', 'Aquaman Description', 'actor01,actor02,actor03', 'director01,director02,director03', 'RZcvMr34', '2019-05-15', 2019, 'Action,Family,Fantasy', '7', 'UK,USA', '155', 'Yes', '', 0),
(83, 'Asuran', 'Asuran', 'asuran-poster.jpg', 'Asuran Description', 'Dhanush,Manju Warrier', 'Vetri Maran,Dhanush', 'AQcv8Ziu123', '2019-10-11', 2019, 'Action,Family,Drama', '8', 'India', '172', 'Yes', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `page_home`
--

CREATE TABLE `page_home` (
  `id` int(5) NOT NULL,
  `home_meta_title` varchar(255) NOT NULL,
  `home_meta_keyword` text NOT NULL,
  `home_meta_description` text NOT NULL,
  `featured_section_title` varchar(255) NOT NULL,
  `movies_section_title` varchar(255) NOT NULL,
  `movies_section_featured_tab_title` varchar(255) NOT NULL,
  `movies_section_top_viewed_tab_title` varchar(255) NOT NULL,
  `movies_section_top_rated_tab_title` varchar(255) NOT NULL,
  `movies_section_recently_added_tab_title` varchar(255) NOT NULL,
  `tv_shows_section_title` varchar(255) NOT NULL,
  `tv_shows_section_featured_tab_title` varchar(255) NOT NULL,
  `tv_shows_section_top_viewed_tab_title` varchar(255) NOT NULL,
  `tv_shows_section_top_rated_tab_title` varchar(255) NOT NULL,
  `tv_shows_section_recently_added_tab_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_home`
--

INSERT INTO `page_home` (`id`, `home_meta_title`, `home_meta_keyword`, `home_meta_description`, `featured_section_title`, `movies_section_title`, `movies_section_featured_tab_title`, `movies_section_top_viewed_tab_title`, `movies_section_top_rated_tab_title`, `movies_section_recently_added_tab_title`, `tv_shows_section_title`, `tv_shows_section_featured_tab_title`, `tv_shows_section_top_viewed_tab_title`, `tv_shows_section_top_rated_tab_title`, `tv_shows_section_recently_added_tab_title`) VALUES
(1, 'Homepage', 'Homepage Meta Keyword12', 'Homepage Meta Description12', 'Featured section title1', 'Movies Section Title1', 'Movies Section Featured Tab1', 'Movies Section Top Viewed Tab1', 'Movies Section Top Rated Tab1', 'Movies Section Recently Added Tab1', 'TV Shows Section Title1', 'TV Shows Section Featured Tab1', 'TV Shows Section Featured Top Viewed Tab1', 'TV Shows Section Top Rated Tab1', 'TV Shows Section Recently Added Tab1');

-- --------------------------------------------------------

--
-- Table structure for table `page_movies`
--

CREATE TABLE `page_movies` (
  `id` int(5) NOT NULL,
  `movies_title` varchar(255) NOT NULL,
  `movies_meta_title` varchar(255) NOT NULL,
  `movies_meta_keyword` text NOT NULL,
  `movies_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_movies`
--

INSERT INTO `page_movies` (`id`, `movies_title`, `movies_meta_title`, `movies_meta_keyword`, `movies_meta_description`) VALUES
(1, 'Movies Page Title1', 'Movies Page Meta Title1', 'Movies Page Meta Keyword1', 'Movies Page Meta Description1');

-- --------------------------------------------------------

--
-- Table structure for table `page_movies_atozlist`
--

CREATE TABLE `page_movies_atozlist` (
  `id` int(5) NOT NULL,
  `movies_atozlist_title` varchar(255) NOT NULL,
  `movies_atozlist_meta_title` varchar(255) NOT NULL,
  `movies_atozlist_meta_keyword` text NOT NULL,
  `movies_atozlist_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_movies_atozlist`
--

INSERT INTO `page_movies_atozlist` (`id`, `movies_atozlist_title`, `movies_atozlist_meta_title`, `movies_atozlist_meta_keyword`, `movies_atozlist_meta_description`) VALUES
(1, 'Movies A to Z List Page Title', 'Movies A to Z List Page Meta Title', 'Movies A to Z List Page Meta Keyword', 'Movies A to Z List Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_movies_country`
--

CREATE TABLE `page_movies_country` (
  `id` int(5) NOT NULL,
  `movies_country_title` varchar(255) NOT NULL,
  `movies_country_meta_title` varchar(255) NOT NULL,
  `movies_country_meta_keyword` text NOT NULL,
  `movies_country_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_movies_country`
--

INSERT INTO `page_movies_country` (`id`, `movies_country_title`, `movies_country_meta_title`, `movies_country_meta_keyword`, `movies_country_meta_description`) VALUES
(1, 'Movies Country Page Title', 'Movies Country Page Meta Title', 'Movies Country Page Meta Keyword', 'Movies Country Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_movies_genre`
--

CREATE TABLE `page_movies_genre` (
  `id` int(5) NOT NULL,
  `movies_genre_title` varchar(255) NOT NULL,
  `movies_genre_meta_title` varchar(255) NOT NULL,
  `movies_genre_meta_keyword` text NOT NULL,
  `movies_genre_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_movies_genre`
--

INSERT INTO `page_movies_genre` (`id`, `movies_genre_title`, `movies_genre_meta_title`, `movies_genre_meta_keyword`, `movies_genre_meta_description`) VALUES
(1, 'Movies Genre Page Title', 'Movies Genre Page Meta Title', 'Movies Genre Page Meta Keyword', 'Movies Genre Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_movie_detail`
--

CREATE TABLE `page_movie_detail` (
  `id` int(5) NOT NULL,
  `movie_title` varchar(255) NOT NULL,
  `movie_meta_title` varchar(255) NOT NULL,
  `movie_meta_keyword` text NOT NULL,
  `movie_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_movie_detail`
--

INSERT INTO `page_movie_detail` (`id`, `movie_title`, `movie_meta_title`, `movie_meta_keyword`, `movie_meta_description`) VALUES
(1, 'Movie Page Title1', 'Movie Page Meta Title1', 'Movie Page Meta Keyword1', 'Movie Page Meta Description1');

-- --------------------------------------------------------

--
-- Table structure for table `page_search`
--

CREATE TABLE `page_search` (
  `id` int(5) NOT NULL,
  `search_title` varchar(255) NOT NULL,
  `search_meta_title` varchar(255) NOT NULL,
  `search_meta_keyword` text NOT NULL,
  `search_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_search`
--

INSERT INTO `page_search` (`id`, `search_title`, `search_meta_title`, `search_meta_keyword`, `search_meta_description`) VALUES
(1, 'Search Page Title', 'Search Page Meta Title', 'Search Page Meta Keyword', 'Search Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_tv_shows`
--

CREATE TABLE `page_tv_shows` (
  `id` int(5) NOT NULL,
  `tv_shows_title` varchar(255) NOT NULL,
  `tv_shows_meta_title` varchar(255) NOT NULL,
  `tv_shows_meta_keyword` text NOT NULL,
  `tv_shows_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_tv_shows`
--

INSERT INTO `page_tv_shows` (`id`, `tv_shows_title`, `tv_shows_meta_title`, `tv_shows_meta_keyword`, `tv_shows_meta_description`) VALUES
(1, 'TV Shows Title1', 'TV Shows | Showtime', 'TV Shows Meta Keyword1', 'Watch Tv Shows Trailer Online Free HD | Stream The Best Tv Series Trailers HD Free Online - Showtime');

-- --------------------------------------------------------

--
-- Table structure for table `page_tv_shows_atozlist`
--

CREATE TABLE `page_tv_shows_atozlist` (
  `id` int(5) NOT NULL,
  `tv_shows_atozlist_title` varchar(255) NOT NULL,
  `tv_shows_atozlist_meta_title` varchar(255) NOT NULL,
  `tv_shows_atozlist_meta_keyword` text NOT NULL,
  `tv_shows_atozlist_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_tv_shows_atozlist`
--

INSERT INTO `page_tv_shows_atozlist` (`id`, `tv_shows_atozlist_title`, `tv_shows_atozlist_meta_title`, `tv_shows_atozlist_meta_keyword`, `tv_shows_atozlist_meta_description`) VALUES
(1, 'TV Shows A to Z List Page Title', 'TV Shows A to Z List Page Meta Title', 'TV Shows A to Z List Page Meta Keyword', 'TV Shows A to Z List Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_tv_shows_country`
--

CREATE TABLE `page_tv_shows_country` (
  `id` int(5) NOT NULL,
  `tv_shows_country_title` varchar(255) NOT NULL,
  `tv_shows_meta_title` varchar(255) NOT NULL,
  `tv_shows_meta_keyword` text NOT NULL,
  `tv_shows_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_tv_shows_country`
--

INSERT INTO `page_tv_shows_country` (`id`, `tv_shows_country_title`, `tv_shows_meta_title`, `tv_shows_meta_keyword`, `tv_shows_meta_description`) VALUES
(1, 'TV Shows Country Page Title', 'TV Shows Country Page Meta Title', 'TV Shows Country Page Meta Keyword', 'TV Shows Country Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_tv_shows_genre`
--

CREATE TABLE `page_tv_shows_genre` (
  `id` int(5) NOT NULL,
  `tv_shows_genre_title` varchar(255) NOT NULL,
  `tv_shows_genre_meta_title` varchar(255) NOT NULL,
  `tv_shows_genre_meta_keyword` text NOT NULL,
  `tv_shows_genre_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_tv_shows_genre`
--

INSERT INTO `page_tv_shows_genre` (`id`, `tv_shows_genre_title`, `tv_shows_genre_meta_title`, `tv_shows_genre_meta_keyword`, `tv_shows_genre_meta_description`) VALUES
(1, 'TV Shows Genre Page Title', 'TV Shows Genre Page Meta Title', 'TV Shows Genre Page Meta Keyword', 'TV Shows Genre Page Meta Description');

-- --------------------------------------------------------

--
-- Table structure for table `page_tv_show_detail`
--

CREATE TABLE `page_tv_show_detail` (
  `id` int(5) NOT NULL,
  `tv_show_title` varchar(255) NOT NULL,
  `tv_show_meta_title` varchar(255) NOT NULL,
  `tv_show_meta_keyword` text NOT NULL,
  `tv_show_meta_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_tv_show_detail`
--

INSERT INTO `page_tv_show_detail` (`id`, `tv_show_title`, `tv_show_meta_title`, `tv_show_meta_keyword`, `tv_show_meta_description`) VALUES
(1, 'TV Show Title1', 'TV Show Meta Title1', 'TV Show Meta Keyword1', 'TV Show Meta Description1');

-- --------------------------------------------------------

--
-- Table structure for table `social_settings`
--

CREATE TABLE `social_settings` (
  `social_id` int(11) NOT NULL,
  `social_name` varchar(30) NOT NULL,
  `social_url` varchar(255) NOT NULL,
  `social_icon` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `social_settings`
--

INSERT INTO `social_settings` (`social_id`, `social_name`, `social_url`, `social_icon`) VALUES
(1, 'Facebook', 'http://www.facebook.com/', 'fa fa-facebook'),
(2, 'Twitter', 'http://www.twitter.com', 'fa fa-twitter'),
(3, 'LinkedIn', 'http://www.linkedin.com', 'fa fa-linkedin'),
(4, 'Google Plus', '', 'fa fa-google-plus'),
(5, 'Pinterest', '', 'fa fa-pinterest'),
(6, 'YouTube', 'http://www.youtube.com', 'fa fa-youtube-play'),
(7, 'Instagram', 'http://www.instagram.com', 'fa fa-instagram'),
(8, 'Tumblr', 'http://www.tumblr.com', 'fa fa-tumblr'),
(9, 'Flickr', '', 'fa fa-flickr'),
(10, 'Reddit', '', 'fa fa-reddit'),
(11, 'Snapchat', '', 'fa fa-snapchat'),
(12, 'WhatsApp', '#', 'fa fa-whatsapp'),
(13, 'Quora', '', 'fa fa-quora'),
(14, 'StumbleUpon', '', 'fa fa-stumbleupon'),
(15, 'Delicious', '', 'fa fa-delicious'),
(16, 'Digg', '', 'fa fa-digg');

-- --------------------------------------------------------

--
-- Table structure for table `tv_shows_info`
--

CREATE TABLE `tv_shows_info` (
  `tv_show_id` int(11) NOT NULL,
  `tv_show_name` varchar(255) NOT NULL,
  `tv_show_original_name` varchar(255) NOT NULL,
  `tv_show_image` varchar(255) NOT NULL,
  `tv_show_description` longtext NOT NULL,
  `tv_show_stars` varchar(255) NOT NULL,
  `tv_show_director` varchar(255) NOT NULL,
  `tv_show_trailer` varchar(255) NOT NULL,
  `tv_show_episodes` varchar(5) NOT NULL,
  `tv_show_start_year` year(4) NOT NULL,
  `tv_show_end_year` varchar(20) NOT NULL,
  `tv_show_genre` varchar(99) NOT NULL,
  `tv_show_rating` varchar(99) NOT NULL,
  `tv_show_country` varchar(255) NOT NULL,
  `tv_show_runtime` varchar(99) NOT NULL,
  `is_featured` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `views_count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tv_shows_info`
--

INSERT INTO `tv_shows_info` (`tv_show_id`, `tv_show_name`, `tv_show_original_name`, `tv_show_image`, `tv_show_description`, `tv_show_stars`, `tv_show_director`, `tv_show_trailer`, `tv_show_episodes`, `tv_show_start_year`, `tv_show_end_year`, `tv_show_genre`, `tv_show_rating`, `tv_show_country`, `tv_show_runtime`, `is_featured`, `status`, `views_count`) VALUES
(1, 'Supernatural 1', '', 'supernatural-poster.jpg', 'Two brothers follow their father''s footsteps as "hunters", fighting evil supernatural beings of many kinds, including monsters, demons, and gods that roam the earth.', '', '', 'mmKLq1InbQc', '22', 2005, '2006', 'Drama,Thriller,Fantasy,Mystery,Horror', '8.5', 'USA', '', 'Yes', '', 7),
(2, 'Breaking Bad', '', 'breaking-bad-poster.jpg', 'A high school chemistry teacher diagnosed with inoperable lung cancer turns to manufacturing and selling methamphetamine in order to secure his family''s future.', '', '', '5NpEA2yaWVQ', '62', 2008, '2013', 'Crime,Drama,Thriller', '9.5', 'USA', '', 'Yes', '', 26),
(3, 'The Walking Dead', '', 'walking-dead-poster.jpg', 'Sheriff Deputy Rick Grimes wakes up from a coma to learn the world is in ruins, and must lead a group of survivors to stay alive.', '', '', 'cu2ApTImBKc', '131*', 2010, 'Live', 'Drama,Horror,Thriller', '8.4', 'USA', '', 'Yes', '', 3),
(4, 'Sherlock', '', 'sherlock-poster.jpg', 'A modern update finds the famous sleuth and his doctor partner solving crime in 21st century London', '', '', 'xK7S9mrFWL4', '15*', 2010, 'Live', 'Crime,Drama,Mystery', '9.2', 'UK,USA', '', 'Yes', '', 3),
(5, 'Game of Thrones', 'Game of Thrones', 'game-of-thrones-poster.jpg', 'Nine noble families fight for control over the mythical lands of Westeros, while an ancient enemy returns after being dormant for thousands of years.', 'Peter Dinklage,Kit Harington,Emilia Clarke', 'David Benioff,D.B.Weiss', 'bjqEWgDVPe0', '73*', 2011, 'Live', 'Action,Adventure,Drama', '9.5', 'UK,USA', '57 min', 'Yes', '', 35),
(6, 'The Big Bang Theory', '', 'the-big-bang-theory-poster.jpg', 'A woman who moves into an apartment across the hall from two brilliant but socially awkward physicists shows them how little they know about life outside of the laboratory.', '', '', 'WBb3fojgW0Q', '280', 2007, 'Live', 'Comedy,Romance', '8.2', 'USA', '', 'Yes', '', 5),
(7, 'Friends', '', 'friends-poster.jpg', 'Follows the personal and professional lives of six 20 to 30-something-year-old friends living in Manhattan.', '', '', 'SHvzX2pl2ec', '236', 1994, '2004', 'Comedy,Romance', '8.9', 'USA', '', 'Yes', '', 4),
(8, 'Dexter', '', 'dexter-poster.jpg', 'By day, mild-mannered Dexter is a blood-spatter analyst for the Miami police. But at night, he is a serial killer who only targets other murderers.', '', '', 'YQeUmSD1c3g', '96', 2006, '2013', 'Crime,Drama,Mystery', '8.7', 'USA', '', 'Yes', '', 2),
(9, 'How I Met Your Mother', '', 'how-i-met-your-mother-poster.jpg', 'A father recounts to his children, through a series of flashbacks, the journey he and his four best friends took leading up to him meeting their mother.', '', '', 'JlhveYg7h0k', '208', 2005, '2014', 'Comedy,Romance', '8.4', 'USA', '', 'Yes', '', 1),
(10, 'Stranger Things', '', 'stranger-things-poster.jpg', '<p>When a young boy disappears, his mother, a police chief, and his friends, must confront terrifying forces in order to get him back.</p>\r\n', '', '', 'XWxyRG_tckY', '25', 2016, 'Live', 'Fantasy, Horror, Drama', '8.9', 'USA', '', 'Yes', '', 9),
(11, 'Lost', '', 'lost-poster.jpg', 'The survivors of a plane crash are forced to work together in order to survive on a seemingly deserted tropical island.', '', '', '_6vudAsjPrA', '118', 2004, '2010', 'Adventure,Drama,Fantasy', '8.4', 'USA', '', 'Yes', '', 4),
(12, 'Prison Break', '', 'prison-break-poster.jpg', 'Due to a political conspiracy, an innocent man is sent to death row and his only hope is his brother, who makes it his mission to deliberately get himself sent to the same prison in order to break the both of them out, from the inside.', '', '', 'mZwEBHc_PBw', '91', 2005, '2017', 'Action,Crime,Drama', '8.4', 'UK,USA', '', 'Yes', '', 2),
(13, 'True Detective', '', 'true-detective-poster.jpg', 'Seasonal anthology series in which police investigations unearth the personal and professional secrets of those involved, both within and outside the law.', '', '', 'fVQUcaO4AvE', '24', 2014, 'Live', 'Crime,Drama,Mystery', '9.0', 'USA', '', 'Yes', '', 12),
(14, 'Arrow', '', 'arrow-poster.jpg', 'Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.', '', '', 'XQS7JkQmlx8', '161', 2012, 'Live', 'Action,Adventure,Crime', '7.7', 'USA', '', 'Yes', '', 3),
(15, 'House', '', 'house-poster.jpg', 'An antisocial maverick doctor who specializes in diagnostic medicine does whatever it takes to solve puzzling cases that come his way using his crack team of doctors and his wits.', '', '', 'MczMB8nU1sY', '176', 2004, '2012', 'Drama,Mystery', '8.8', 'USA', '', 'Yes', '', 1),
(16, 'Suits', '', 'suits-poster.jpg', 'On the run from a drug deal gone bad, Mike Ross, a brilliant college dropout, finds himself a job working with Harvey Specter, one of New York City''s best lawyers.', '', '', 'IaqKmCpbJvM', '109', 2011, 'Live', 'Comedy,Drama', '8.6', 'USA', '', 'Yes', '', 1),
(17, 'Vikings', '', 'vikings-poster.jpg', 'The world of the Vikings is brought to life through the journey of Ragnar Lothbrok, the first Viking to emerge from Norse legend and onto the pages of history - a man on the edge of myth.', '', '', 'xdm7Z3TQhDg', '90', 2013, 'Live', 'Action,Adventure,Drama', '8.6', 'Ireland,Canada', '', 'Yes', '', 1),
(18, 'Westworld', '', 'westworld-poster.jpg', 'Set at the intersection of the near future and the reimagined past, explore a world in which every human appetite can be indulged without consequence.', '', '', 'IuS5huqOND4', '22', 2016, 'Live', 'Drama,Mystery,Sci-Fi', '8.9', 'USA', '', 'Yes', '', 5),
(19, 'Homeland', '', 'homeland-poster.jpg', 'A bipolar CIA operative becomes convinced a prisoner of war has been turned by al-Qaeda and is planning to carry out a terrorist attack on American soil.', '', '', 'KyFmS3wRPCQ', '96', 2011, 'Live', 'Crime,Drama,Mystery', '8.4', 'USA', '', 'Yes', '', 4),
(20, 'Arrested Development', '', 'arrested-development-poster.jpg', '<p>Level-headed son Michael Bluth takes over family affairs after his father is imprisoned. But the rest of his spoiled, dysfunctional family are making his job unbearable.</p>\r\n', '', '', 'kM5O9LC2xa8', '84*', 2003, 'Live', 'Comedy', '8.9', 'USA', '', 'Yes', '', 12);

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE `users_info` (
  `id` int(11) NOT NULL,
  `username` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `role` varchar(30) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`id`, `username`, `email`, `password`, `photo`, `token`, `role`, `active`) VALUES
(1, 'admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', 'Administrator', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners_info`
--
ALTER TABLE `banners_info`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `base_setup`
--
ALTER TABLE `base_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_info`
--
ALTER TABLE `country_info`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `general_settings`
--
ALTER TABLE `general_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `genre_info`
--
ALTER TABLE `genre_info`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indexes for table `movies_info`
--
ALTER TABLE `movies_info`
  ADD PRIMARY KEY (`movie_id`);

--
-- Indexes for table `page_home`
--
ALTER TABLE `page_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_movies`
--
ALTER TABLE `page_movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_movies_atozlist`
--
ALTER TABLE `page_movies_atozlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_movies_country`
--
ALTER TABLE `page_movies_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_movies_genre`
--
ALTER TABLE `page_movies_genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_movie_detail`
--
ALTER TABLE `page_movie_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_search`
--
ALTER TABLE `page_search`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_tv_shows`
--
ALTER TABLE `page_tv_shows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_tv_shows_atozlist`
--
ALTER TABLE `page_tv_shows_atozlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_tv_shows_country`
--
ALTER TABLE `page_tv_shows_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_tv_shows_genre`
--
ALTER TABLE `page_tv_shows_genre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_tv_show_detail`
--
ALTER TABLE `page_tv_show_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_settings`
--
ALTER TABLE `social_settings`
  ADD PRIMARY KEY (`social_id`);

--
-- Indexes for table `tv_shows_info`
--
ALTER TABLE `tv_shows_info`
  ADD PRIMARY KEY (`tv_show_id`);

--
-- Indexes for table `users_info`
--
ALTER TABLE `users_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners_info`
--
ALTER TABLE `banners_info`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `base_setup`
--
ALTER TABLE `base_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `country_info`
--
ALTER TABLE `country_info`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `general_settings`
--
ALTER TABLE `general_settings`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `genre_info`
--
ALTER TABLE `genre_info`
  MODIFY `genre_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `movies_info`
--
ALTER TABLE `movies_info`
  MODIFY `movie_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `page_home`
--
ALTER TABLE `page_home`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_movies`
--
ALTER TABLE `page_movies`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_movies_atozlist`
--
ALTER TABLE `page_movies_atozlist`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_movies_country`
--
ALTER TABLE `page_movies_country`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_movies_genre`
--
ALTER TABLE `page_movies_genre`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_movie_detail`
--
ALTER TABLE `page_movie_detail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_search`
--
ALTER TABLE `page_search`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_tv_shows`
--
ALTER TABLE `page_tv_shows`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_tv_shows_atozlist`
--
ALTER TABLE `page_tv_shows_atozlist`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_tv_shows_country`
--
ALTER TABLE `page_tv_shows_country`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_tv_shows_genre`
--
ALTER TABLE `page_tv_shows_genre`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `page_tv_show_detail`
--
ALTER TABLE `page_tv_show_detail`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `social_settings`
--
ALTER TABLE `social_settings`
  MODIFY `social_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tv_shows_info`
--
ALTER TABLE `tv_shows_info`
  MODIFY `tv_show_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
