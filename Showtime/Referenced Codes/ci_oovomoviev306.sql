-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2019 at 03:22 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_oovomoviev306`
--

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `genre_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(250) NOT NULL,
  `slug` varchar(128) NOT NULL,
  `publication` int(1) NOT NULL,
  `featured` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genre_id`, `name`, `description`, `slug`, `publication`, `featured`) VALUES
(1, 'Action', 'Action Movie<br>', 'action', 1, 1),
(2, 'TV Show', 'Tv Show <br>', 'tv-show', 1, 0),
(3, 'Si-Fi', '', 'si-fi', 1, 0),
(4, 'Adventure', 'Adventure Movies<br>', 'adventure', 1, 0),
(5, 'Animation', 'Animation Movies<br>', 'animation', 1, 0),
(6, 'Biography', 'Biography Movies<br>', 'biography', 1, 0),
(7, 'Comedy', 'Comedy Movies<br>', 'comedy', 1, 1),
(8, 'Crime', 'Crime Movies<br>', 'crime', 1, 0),
(9, 'Documentary', 'Documentary Movies<br>', 'documentary', 1, 0),
(10, 'Drama', '', 'drama', 1, 0),
(11, 'Family', 'Family<br>', 'family', 1, 0),
(12, 'Fantasy', 'Fantasy Movies<br>', 'fantasy', 1, 0),
(13, 'History', '', 'history', 1, 0),
(14, 'Horror', 'Horror Movies<br>', 'horror', 1, 1),
(15, 'Music', '', 'music', 1, 0),
(16, 'Musical', '', 'musical', 1, 0),
(17, 'Mystery', '', 'mystery', 1, 0),
(18, 'Thriller', '', 'thriller', 1, 1),
(19, 'War', '', 'war', 1, 0),
(20, 'Western', '', 'western', 1, 0),
(21, 'TV Series', '', 'tv-series', 1, 0),
(22, ' Romance', ' Romance', 'romance', 1, 0),
(23, ' Adventure', ' Adventure', 'adventure', 1, 0),
(24, ' Thriller', ' Thriller', 'thriller', 1, 0),
(25, ' Drama', ' Drama', 'drama', 1, 0),
(26, ' Fantasy', ' Fantasy', 'fantasy', 1, 0),
(27, ' Sci-Fi', ' Sci-Fi', 'sci-fi', 1, 0),
(28, ' Comedy', ' Comedy', 'comedy', 1, 0),
(29, ' Family', ' Family', 'family', 1, 0),
(30, ' Action', ' Action', 'action', 1, 0),
(31, 'Short', 'Short', 'short', 1, 0),
(32, ' Music', ' Music', 'music', 1, 0),
(33, ' History', ' History', 'history', 1, 0),
(34, ' Crime', ' Crime', 'crime', 1, 0),
(35, ' Western', ' Western', 'western', 1, 0),
(36, ' Sport', ' Sport', 'sport', 1, 0),
(37, ' Short', ' Short', 'short', 1, 0),
(38, ' Mystery', ' Mystery', 'mystery', 1, 0),
(39, 'Romance', 'Romance', 'romance', 1, 0),
(40, 'Action & Adventure', 'Action & Adventure', 'action-adventure', 1, 0),
(41, 'Sci-Fi & Fantasy', 'Sci-Fi & Fantasy', 'sci-fi-fantasy', 1, 0),
(42, 'Science Fiction', 'Science Fiction', 'science-fiction', 1, 0),
(44, 'TV Movie', 'TV Movie', 'tv-movie', 1, 0),
(45, 'News', 'News', 'news', 1, 0),
(46, '', '', '', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `videos_id` int(11) NOT NULL,
  `imdbid` varchar(200) DEFAULT NULL,
  `title` varchar(150) NOT NULL,
  `seo_title` varchar(250) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `description` longtext,
  `stars` varchar(250) DEFAULT '',
  `director` varchar(250) DEFAULT NULL,
  `writer` varchar(250) DEFAULT NULL,
  `rating` varchar(5) DEFAULT '0',
  `release` varchar(25) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `genre` varchar(200) DEFAULT NULL,
  `video_type` varchar(50) DEFAULT NULL,
  `runtime` varchar(10) DEFAULT NULL,
  `video_quality` varchar(200) DEFAULT 'HD',
  `publication` int(5) DEFAULT NULL,
  `trailer` int(5) DEFAULT '0',
  `enable_download` int(5) DEFAULT '1',
  `focus_keyword` text,
  `meta_description` text,
  `tags` text,
  `imdb_rating` varchar(5) DEFAULT NULL,
  `is_tvseries` int(11) NOT NULL DEFAULT '0',
  `total_rating` int(50) DEFAULT '1',
  `today_view` int(250) DEFAULT '0',
  `weekly_view` int(250) DEFAULT '0',
  `monthly_view` int(250) DEFAULT '0',
  `total_view` int(250) DEFAULT '1',
  `last_ep_added` datetime DEFAULT '2019-04-04 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`videos_id`, `imdbid`, `title`, `seo_title`, `slug`, `description`, `stars`, `director`, `writer`, `rating`, `release`, `country`, `genre`, `video_type`, `runtime`, `video_quality`, `publication`, `trailer`, `enable_download`, `focus_keyword`, `meta_description`, `tags`, `imdb_rating`, `is_tvseries`, `total_rating`, `today_view`, `weekly_view`, `monthly_view`, `total_view`, `last_ep_added`) VALUES
(1, 'tt0101649', 'Thalapathi', '', 'thalapathi', '<p>Surya is an orphan raised in a slum. He finds a friend in the local Godfather. They rule the town and forms a parallel government. Things are well until new district collect, step -brother of Surya takes charge. Surya finds his mom and decides to surrender.</p>', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18', '19', '19,19,24', '0', '1991-11-05', '10', '1,8,10', NULL, '155 Min', '4K', 1, 0, 0, '', '', '', '7.8', 0, 1, 9, 9, 9, 10, '2019-04-04 00:00:00'),
(2, 'tt7329602', 'Tamizh Padam 2', '', 'tamizh-padam-2', '<p>A stringent cop tries all the possible ways to nab a dreaded don who poses a big threat to the society.</p>', '32,33,34,35,36,37,72,39,40,41,42,73,44,45,46,47,74', '75', '75,75,76', '0', '2018-07-12', '10', '7', NULL, '145 Min', '4K', 1, 0, 0, '', '', '', '6.1', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(3, 'tt0140377', 'Michael Madana Kamarajan', '', 'michael-madana-kamarajan', '<p>The film tells a story of quadruplets, Michael, Madhanagopal, Kameshwaran, and Raju, all played by Kamal Haasan. Kamal had distinguished each of those characters with his body language and language lingo. Michael has a husky voice, Madan has an English accent, Kameshwaran speaks Palakkad Tamil and Raju speaks Madras Tamil in keeping with their diverse upbringing as per the plot.</p>', '77,78,79,39,80,81,90,83,84,10,85,86,91,88', '89', '77', '0', '1990-03-01', '10', '7', NULL, '167 Min', '4K', 1, 0, 0, '', '', '', '6.8', 0, 1, 1, 1, 1, 2, '2019-04-04 00:00:00'),
(4, '', 'Supernatural', '', 'supernatural', '<p>When they were boys, Sam and Dean Winchester lost their mother to a mysterious and demonic supernatural force. Subsequently, their father raised them to be soldiers. He taught them about the paranormal evil that lives in the dark corners and on the back roads of America ... and he taught them how to kill it. Now, the Winchester brothers crisscross the country in their ''67 Chevy Impala, battling every kind of supernatural threat they encounter along the way. </p>', '92,93,94,95', '96', '96', '0', '2005-09-13', NULL, '10,17,41', NULL, '', '4K', 1, 0, 0, '', '', '', '7.4', 1, 1, 2, 2, 2, 3, '2019-04-04 00:00:00'),
(5, 'tt1496729', 'Aaranya Kaandam', 'Aaranya Kaandam', 'aaranya-kaandam', 'A drama that unfolds between two rival mafia gang on a single day when a bag of cocaine gets stolen by two innocent people.', '97,98,99,100,101,102,103,104,58', '106', '106', '0', '2011-06-10', '10', '7,1,8,10', NULL, '153 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.6', 0, 2, 1, 1, 1, 2, '2019-04-04 00:00:00'),
(6, 'tt0459516', 'Pudhupettai', 'Pudhupettai', 'pudhupettai', 'The story is set in the backdrop of the slums of Pudhupettai in Chennai, as the title suggests, where a high school kid Kokki Kumar (Dhanush) sees his mom dead after he comes back from watching a movie. His father, the murderer is also planning to kill his son Kumar. Kumar sensing danger runs away from home. Homeless and with no food Kumar resorts to begging until he is falsely arrested while standing by and watching a commotion on the street. In jail he befriends handymen of a local goon who take Kumar with them and give him petty jobs to do. In a confrontation with goons of another gang Kumar kills the brother of his enemy gang''s head Moorthi thus earning the respect of his gang and making enemies for himself both within his gang and outside.', '113,114,126,116,117,118', '127', '120,127', '0', '2006-05-26', '10', '1,10,8', NULL, '170 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.2', 0, 1, 2, 2, 2, 3, '2019-04-04 00:00:00'),
(7, '', 'Pudhu Vasantham', 'Pudhu Vasantham', 'pudhu-vasantham', 'Pudhu Vasantham is a 1990 Indian Tamil film, directed by Vikraman making his directorial debut, starring Murali, Anand Babu, Raja, Charle and Sithara in lead roles.', '128,129,130,131,132,133,134,135,155,137,156,139,140,141,142,143,144,145,146,147,157', '149', '149', '0', '1990-04-14', '10', '46', NULL, ' Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(8, 'tt0155976', 'Pudhu Nellu Pudhu Naathu', 'Pudhu Nellu Pudhu Naathu', 'pudhu-nellu-pudhu-naathu', 'Pudhu Nellu Pudhu Naathu is a Tamil film directed by P. Bharathiraja. The music composed by maestro Ilaiyaraaja while lyrics written by Muthulingham, Ilaiyaraaja and Gangai Amaran. Two peasant brothers clash with a powerful landlord.', '158,159,160,161,162,163,164', '169', '159,170,169', '0', '1991-03-15', '10', '39,10,1,11', NULL, '143 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 0, 1, 1, 1, 1, 2, '2019-04-04 00:00:00'),
(9, 'tt0155064', 'Pudhu Pudhu Arthangal', 'Pudhu Pudhu Arthangal', 'pudhu-pudhu-arthangal', 'Pudhu Pudhu Arthangal is a 1989 Tamil film directed by K. Balachander and produced by Rajam Balachander and Pushpa Kandaswamy. The film stars Rahman, Sithara, Geetha and Janagaraj in lead roles.Music was composed by  and Ilaiyaraaja.', '171,132,5,172,173,174,175,176,184,22,178', '185', '185', '0', '1989-10-28', '10', '11,10,39', NULL, '151 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(10, '', 'Pudhumai Pithan', 'Pudhumai Pithan', 'pudhumai-pithan', 'Pudhumai Pithan is a 1998 Tamil film directed by Jeeva. The film stars Parthiban, Roja, Devayani and Priya Raman in lead roles.', '186,187,188,189,190,191,135,192,204,131,80,194,195,196', '197', '200,197,205', '0', '1998-10-20', '10', '46', NULL, ' Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(11, 'tt0093603', 'Nayakan', 'Nayakan', 'nayakan', 'Sakthivelu Nayakar (Kamal Hassan), the protagonist, is born to an anti-government union leader. The child Velu is tricked by the police into locating his father...', '77,206,172,207,208,221,80,36,85,210,211,222,223,224,215,216,13,217,218', '19', '19,120', '0', '1987-07-01', '10', '10,1', NULL, '145 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(12, 'tt8176054', 'Pariyerum Perumal', 'Pariyerum Perumal', 'pariyerum-perumal', 'A law student from a lower caste begins a friendship with his classmate, a girl who belongs to a higher caste, and the men in her family start giving him trouble over this.', '225,226,227,250,229,230,231,232,251,234,235', '237', '237,237', '0', '2018-09-28', '10', '39,10', NULL, '155 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '8.3', 0, 1, 5, 5, 5, 6, '2019-04-04 00:00:00'),
(13, 'tt0105575', 'Thevar Magan', 'Thevar Magan', 'thevar-magan', 'Thevar Magan (English: Son of the Thevar) is a 1992 Indian Tamil film produced by, written by, and starring Kamal Hassan in the title role. It was directed by Bharathan and also stars Sivaji Ganesan, Nasser, Revathi and Gouthami in pivotal roles. The film met with a very strong critical and commercial reception upon release. The film score and soundtrack are composed by Ilaiyaraaja. India chose this film as its entry for the Best Foreign Language Film for the Academy Awards in 1992. The film was screened at the Toronto Film Festival in 1994.  The film won five National Film Awards, including the Best Tamil Film Award, Best Supporting Actress Award, and a Special Jury Award.  The film was dubbed into Telugu under the title Kshatriya Putrudu. It was later remade into the Hindi film Virasat (1997) by Priyadarshan and in Kannada as Tandege Takka Maga (2006) by S. Mahendar', '77,252,253,254,192,85,90,255,256,257,258', '259', '77', '0', '1992-05-05', '10', '10', NULL, '160 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(14, '', 'Planet Earth', 'Planet Earth', 'planet-earth', 'David Attenborough celebrates the amazing variety of the natural world in this epic documentary series, filmed over four years across 64 different countries.', '262', '263', '268,270', '0', '2006-03-05', '85', '9', NULL, '', 'HD', 1, 0, 0, NULL, NULL, NULL, '8.2', 1, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(15, '', 'Human Planet', 'Human Planet', 'human-planet', 'A cinematic experience bringing you the most amazing human stories in the world. Humans and wildlife surviving in the most extreme environments on Earth.', '271', '96', '96', '0', '2011-01-13', '85', '9', NULL, '', 'HD', 1, 0, 0, NULL, NULL, NULL, '8.3', 1, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(16, '', 'Captain Planet and the Planeteers', 'Captain Planet and the Planeteers', 'captain-planet-and-the-planeteers', 'Captain Planet and the Planeteers is an American animated environmentalist television program created by Ted Turner, Robert Larkin III, and Barbara Pyle, produced by Pyle, Nicholas Boxer, Andy Heyward and Robby London, and developed by Pyle, Boxer, Heyward, London, Thom Beers, Bob Forward, Phil Harnage and Cassandra Schafhausen. The series was produced by Turner Program Services and DIC Entertainment and it was broadcast on TBS from September 15, 1990 to December 5, 1992. A sequel series, The New Adventures of Captain Planet, was produced by Hanna-Barbera and Turner Program Services, and was broadcast from September 11, 1993 to May 11, 1996. Both series continue today in syndication. The program is a form of edutainment and advocates environmentalism.\n\nIn February 2009, Mother Nature Network began airing episodes and unreleased footage of Captain Planet and the Planeteers on its website. In September 2010, the Planeteer Movement was launched with the assistance of Pyle as a means for fans of the show to connect and continue to integrate the show''s messages into their lives as real-life Planeteers.', '272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290', '96', '96', '0', '1990-09-15', '84', '5,40', NULL, '', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.2', 1, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(17, '', 'Seven Worlds, One Planet', 'Seven Worlds, One Planet', 'seven-worlds-one-planet', 'Millions of years ago, incredible forces ripped apart the Earth’s crust creating seven extraordinary continents. This documentary series reveals how each distinct continent has shaped the unique animal life found there.', '262', '96', '96', '0', '2019-10-27', '85', '9', NULL, '', 'HD', 1, 0, 0, NULL, NULL, NULL, '9.3', 1, 1, 1, 1, 1, 2, '2019-04-04 00:00:00'),
(18, '', 'Miracle Planet', 'Miracle Planet', 'miracle-planet', 'Flora and fauna, sea and stone - the elements of life on earth as we know it. But how did the complicated existence on Earth come to be?', '302', '303', '96', '0', '2005-04-22', '86', '9', NULL, '', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 1, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(19, 'tt0107362', 'Last Action Hero', 'Last Action Hero', 'last-action-hero', 'Danny is obsessed with a fictional movie character action hero Jack Slater. When a magical ticket transports him into Jack''s latest adventure, Danny finds himself in a world where movie magic and reality collide. Now it''s up to Danny to save the life of his hero and new friend.', '304,500,501,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,502,323,324,503,326,327,328,329,330,331,332,333,334,335,336,337,338,504,505,341,342,343,506,345,346,507,348,349,350,351,352,353,354,355,356,357,508,359,509,361,510', '364', '365,388,392,393', '0', '1993-06-18', '68', '4,12,1,7,11', NULL, '130 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(20, 'tt6495770', 'Action Point', 'Action Point', 'action-point', 'A daredevil designs and operates his own theme park with his friends.', '511,512,551,514,515,516,517,518,519,520,552,522,523,524,525,526,527,528,529,530,531,532,533,534,553,536', '542', '511,540', '0', '2018-06-01', '68,72', '7,10', NULL, '85 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.2', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(21, 'tt0094612', 'Action Jackson', 'Action Jackson', 'action-jackson', 'Vengence drives a tough Detroit cop to stay on the trail of a power hungry auto magnate who''s systematically eliminating his competition.', '554,580,556,330,581,558,559,560,561,562,563,582,583,566,567,568,569,570,571,332,572,573', '584', '578', '0', '1988-02-12', '68', '1,4,7,8,10', NULL, '96 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(22, 'tt0120633', 'A Civil Action', 'A Civil Action', 'a-civil-action', 'Jan Schlickmann is a cynical lawyer who goes out to ''get rid of'' a case, only to find out it is potentially worth millions. The case becomes his obsession, to the extent that he is willing to give up everything—including his career and his clients'' goals—in order to continue the case against all odds.', '585,586,587,617,589,590,591,592,593,594,595,596,597,598,599,600,601,602,603', '607', '607', '0', '1998-12-25', '68', '10', NULL, '115 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(23, 'tt0089604', 'Missing in Action 2: The Beginning', 'Missing in Action 2: The Beginning', 'missing-in-action-2-the-beginning', 'Prequel to the first Missing In Action, set in the early 1980s it shows the capture of Colonel Braddock during the Vietnam war in the 1970s, and his captivity with other American POWs in a brutal prison camp, and his plans to escape.', '618,647,620,621,622,623,624,625,333,626,627,628,629,630,631,632,633,634,635,636', '637', '644,645,646', '0', '1985-03-01', '68', '1,19', NULL, '100 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.5', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(24, 'tt0094792', 'Braddock: Missing in Action III', 'Braddock: Missing in Action III', 'braddock-missing-in-action-iii', 'When Colonel James Braddock is told that his Asian wife and 12-year-old son are still alive in Communist Vietnam, he mounts a one-man assault to free them. Armed with the latest high-tech firepower, Braddock fights his way into the heart of the country and ends up battling his way out with several dozen abused Amerasian children in tow! Struggling to keep them alive while outmaneuvering a sadistic Vietnamese officer, Braddock ignites the jungle in a blazing cross-country race for freedom.', '618,648,649,650,651,652,653,654,655,656,657,658,659,660,661,662,663,636', '664', '665,618,644,645,646', '0', '1988-01-22', '68', '1,19', NULL, '101 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(25, 'tt0318155', 'Looney Tunes: Back in Action', 'Looney Tunes: Back in Action', 'looney-tunes-back-in-action', 'Fed up with all the attention going to Bugs Bunny, Daffy Duck quits Hollywood, teams up with recently-fired stuntman Damien Drake Jr. and embarks on a round-the-world adventure, along with Bugs and The VP of Warner Bros. Their mission? Find Damien''s father, and the missing blue diamond... and stay one step ahead of The Acme Corp., who wants the diamond for their own purposes.', '670,671,672,673,674,675,676,677,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,272,698,792,700,701,793,703,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728', '732', '744,754,773,774', '0', '2003-11-14', '74,68', '5,7,11', NULL, '93 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(26, 'tt1396208', 'Action Replayy', 'Action Replayy', 'action-replayy', 'A young man tries to revive his parents'' wilting marriage in a unique manner - travel to the 1970s when their romance was budding and make it bloom. This is more complex than he expects.', '794,795,796,797,798,799,800', '801', '802,803,801', '0', '2010-11-05', '10', '7,39,42', NULL, '100 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.2', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(27, 'tt0078732', 'Action', 'Action', 'action', 'Bruno is an idealistic hero who questions the meaning of life in this confusing and sometimes hallucinatory erotic drama. After a night in jail, he is gang-raped by punk rockers in a garbage dump. He later saves an old man who believes he is Garibaldi and a woman he believes is Ophelia. Bruno watches helplessly as she later jumps from a window.', '804,805,806,807,808,809,810,811,812,813,814,815,816,817,818,823,824,821,822', '822', '822', '0', '1980-01-04', '12', '10', NULL, '121 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '4.1', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(28, 'tt7656570', 'Legal Action', 'Legal Action', 'legal-action', 'Big-city lawyer Casey McKay, is drawn to a small town by his ex-wife to defend her brother, accused of murdering a DA. He discovers a web of conspiracy that puts him face-to-face with the town''s most corrupt land developer, Mr. Gates.', '825,826,827,828,829,830,831,832,833', '834', '835,836', '0', '2018-10-08', '68', '1,20', NULL, '97 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(29, 'tt1206885', 'Rambo: Last Blood', 'Rambo: Last Blood', 'rambo-last-blood', 'When John Rambo''s niece travels to Mexico to find the father that abandoned her and her mother, she finds herself in the grasps of Calle Mexican sex traffickers. When she doesn''t return home as expected, John learns she''s crossed into Mexico and sets out to get her back and make them pay.', '837,838,873,874,841,875,843,844,845,846,847,876,849,850,851', '864', '837,856,869', '0', '2019-09-19', '68', '1,18', NULL, '101 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.9', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(30, 'tt0462499', 'Rambo', 'Rambo', 'rambo', 'When governments fail to act on behalf of captive missionaries, ex-Green Beret John James Rambo sets aside his peaceful existence along the Salween River in a war-torn region of Thailand to take action.  Although he''s still haunted by violent memories of his time as a U.S. soldier during the Vietnam War, Rambo can hardly turn his back on the aid workers who so desperately need his help.', '837,877,878,879,880,881,882,883,884,885,886,887,888,889,1050,891,892,893,894', '837', '837,856,910', '0', '2008-01-24', '74,68', '1,18', NULL, '92 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.5', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(31, 'tt0095956', 'Rambo III', 'Rambo III', 'rambo-iii', 'Combat has taken its toll on Rambo, but he''s finally begun to find inner peace in a monastery. When Rambo''s friend and mentor Col. Trautman asks for his help on a top secret mission to Afghanistan, Rambo declines but must reconsider when Trautman is captured.', '837,1051,1052,1168,1054,1055,1056,1057,1058,1059,1060,1061,1062,1063,1169,1065,1066,1067,1068,1069,1070,1071,1072,1170,1074,1075', '1090', '837,856,1091', '0', '1988-05-24', '68', '1,4,18,19', NULL, '102 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.9', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(32, 'tt0089880', 'Rambo: First Blood Part II', 'Rambo: First Blood Part II', 'rambo-first-blood-part-ii', 'John Rambo is released from prison by the government for a top-secret covert mission to the last place on Earth he''d want to return - the jungles of Vietnam.', '837,1051,1171,1172,1173,1174,1175,1176,1177,1178,1179,1180,1181,1182,1183,1184,1185,1186,1108,1187', '1258', '1190,837,856', '0', '1985-05-21', '87,68', '1,4,18,19', NULL, '96 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(33, 'tt6912162', 'Rambo', 'Rambo', 'rambo-33', 'The action film will follow the last surviving member of an elite covert unit of the Indian Armed forces who returns home to discover a war waging in his own land. Forced into the dangerous jungles and frozen mountains of the Himalayas, he unleashes mayhem and destruction, becoming the unstoppable machine he was trained to be.', '1259', '1260', '96', '0', '2020-10-02', '10', '1', NULL, ' Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '0', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(34, 'tt0083944', 'First Blood', 'First Blood', 'first-blood', 'When former Green Beret John Rambo is harassed by local law enforcement and arrested for vagrancy, the Vietnam vet snaps, runs for the hills and rat-a-tat-tats his way into the action-movie hall of fame. Hounded by a relentless sheriff, Rambo employs heavy-handed guerilla tactics to shake the cops off his tail.', '837,1051,1266,687,1267,1268,1269,1270,1271,1272,1339,1274,1340,1276,1277,1278,1341,1280,1281,1282,1283,1284,1285', '1290', '837,856,856,1291,1292', '0', '1982-10-22', '68', '1,4,18,19', NULL, '93 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(35, 'tt0458339', 'Captain America: The First Avenger', 'Captain America: The First Avenger', 'captain-america-the-first-avenger', 'During World War II, Steve Rogers is a sickly man from Brooklyn who''s transformed into super-soldier Captain America to aid in the war effort. Rogers must stop the Red Skull – Adolf Hitler''s ruthless head of weaponry, and the leader of an organization that intends to use a mysterious device of untold powers for world domination.', '1342,1343,1344,1345,1346,1347,1348,1349,1543,1351,1352,1353,1354,1544,1356,1357,1358,1359,1360,1361,1362,1363,1364,1365,1366,1367,1368,1545,1370,1371,1372,1373,1374,1375,1376,1377,1378,1379,1380,1381,1382,1383,1384,1385,1386,1387,1388,1389,1390,1391,', '1403', '1407,1408,1530', '0', '2011-07-22', '68', '1,4,42', NULL, '124 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.9', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(36, 'tt4154796', 'Avengers: Endgame', 'Avengers: Endgame', 'avengers-endgame', 'After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos'' actions and restore order to the universe once and for all, no matter what consequences may be in store.', '1906,1342,1548,1549,1550,1551,1552,1553,1554,1555,1556,1557,1558,1559,1560,1561,1562,1563,1564,1565,1344,1566,1567,1568,1569,1570,1571,1572,1573,1574,1575,1343,1576,1577,1578,1579,1580,1581,1582,1583,1584,1585,609,1586,1587,1543,1588,1589,1590,1907,1', '1650,1599', '1407,1408,1397,1649,1626', '0', '2019-04-24', '68', '4,42,1', NULL, '181 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '8.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(37, 'tt4154756', 'Avengers: Infinity War', 'Avengers: Infinity War', 'avengers-infinity-war', 'As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.', '1906,1549,1342,1548,1550,1555,1558,1556,1552,1560,1559,1564,1912,1565,1344,1566,1913,1567,1914,1568,1569,1570,1583,1584,1585,1915,1586,1587,1588,1582,1571,1601,1908,1916,1600,1397,1589,1917,1602,1625,1596,1918,1919,1920,1921,1638,1598,1922,1408,1634,', '1650,1599', '1407,1408,1649,1626', '0', '2018-04-25', '68', '4,1,42', NULL, '149 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '8.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(38, 'tt0848228', 'The Avengers', 'The Avengers', 'the-avengers', 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!', '1906,1342,1549,1548,1550,1551,1566,1543,1574,2104,2332,1585,1912,2106,2107,2108,2109,2110,2333,2112,2113,2114,2115,2116,2117,2118,1907,2119,2120,2121,2122,2123,2124,2125,2126,2127,2128,2129,2130,2131,1620,2132,2133,2334,2135,2136,2137,1397,2138,2139,', '2216', '1397,2216,2216,388,1649,1530', '0', '2012-04-25', '68', '42,1,4', NULL, '143 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.7', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(39, 'tt2395427', 'Avengers: Age of Ultron', 'Avengers: Age of Ultron', 'avengers-age-of-ultron', 'When Tony Stark tries to jumpstart a dormant peacekeeping program, things go awry and Earth’s Mightiest Heroes are put to the ultimate test as the fate of the planet hangs in the balance. As the villainous Ultron emerges, it is up to The Avengers to stop him from enacting his terrible plans, and soon uneasy alliances and unexpected action pave the way for an epic and unique global adventure.', '1906,1342,1549,1548,1550,1551,2341,1543,1552,2443,1564,1912,1574,1565,1343,1913,1590,2332,2343,2344,2345,2346,1397,2347,2348,1621,2444,2350,2351,2352,2353,2354,2355,2356,2357,2358,2359,2360,2361,2362,2363,2364,2365,2366,2367,1603,2368,2369,2370,2371,', '2216', '1397,2216,1649,1530', '0', '2015-04-22', '68', '1,4,42', NULL, '141 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '7.3', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(40, 'tt0286716', 'Hulk', 'Hulk', 'hulk', 'Bruce Banner, a genetics researcher with a tragic past, suffers massive radiation exposure in his laboratory that causes him to transform into a raging green monster when he gets angry.', '2449,2450,2451,2452,2453,2454,2455,2456,2457,2458,2459,2141,1397,2460,2461,2462,2463,2464,2465,2466,2467,2468,2469,2470,2471,2472,2473,2474,2475,2476,2477,2478,2479,2480,2481,2482,2483,2662,2485,2663,2487,2488,2489,2490,2664,2492,2493,2494,2495,2496,', '2500', '2501,2501,1397,2509,1649,2533', '0', '2003-06-19', '68', '1,42', NULL, '138 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(41, 'tt0800080', 'The Incredible Hulk', 'The Incredible Hulk', 'the-incredible-hulk', 'Scientist Bruce Banner scours the planet for an antidote to the unbridled force of rage within him: the Hulk. But when the military masterminds who dream of exploiting his powers force him back to civilization, he finds himself coming face to face with a new, deadly foe.', '2665,2666,2667,1582,2668,2669,2670,2671,2141,2672,2772,2674,2675,2676,2677,2678,2679,2680,2773,2682,2683,2684,2685,2686,2687,2688,2689,2690,2774,2692,2693,2694,2695,2696,2697,2698,2699,2700,2701,2702,2703,2704,2705,2706,2707,2708,2709,2775,2776,2712,', '2736', '1397,388,1649', '0', '2008-06-12', '68', '42,1,4', NULL, '114 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.2', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(42, 'tt0076190', 'The Incredible Hulk', 'The Incredible Hulk', 'the-incredible-hulk-42', 'A troubled scientist''s accidental overexposure to gamma radiation curses him with the tendency to change into a bestial green brute under extreme emotional stress.', '2778,2141,2779,2780,2781,2782,2783,2784,2785,2786,2787,2788,2789,2790', '2791', '2791', '0', '1977-11-04', '68', '1,42,44', NULL, '95 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '6.4', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00'),
(43, 'tt0098512', 'The Trial of the Incredible Hulk', 'The Trial of the Incredible Hulk', 'the-trial-of-the-incredible-hulk', 'On the run again, Dr. David Banner is jailed for assault after interrupting a mugging. Blind attorney Matt Murdock enlists Banner''s help in locating the muggers because he believes they work for his longtime foe, Fisk, the head of an international crime network. But David, afraid of public exposure, breaks out of jail as the Hulk. Tracking David down, Murdock reveals his own secret: His blindness came from a radioactive spill, and after developing his other senses so incredibly, he has become the amazingly athletic crime fighter called Daredevil. Fisk must now face off against Daredevil and the Incredible Hulk!', '2778,2141,2818,2793,2794,2795,2819,2797,2798,2799,2800,2801,2802,2803,2804,1397', '2778', '1397,2808', '0', '1989-05-06', '68', '4,12,10,1,42', NULL, '100 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '5.5', 0, 1, 1, 1, 1, 2, '2019-04-04 00:00:00'),
(44, 'tt0099387', 'The Death of the Incredible Hulk', 'The Death of the Incredible Hulk', 'the-death-of-the-incredible-hulk', 'During the critical experiment that would rid David Banner of the Hulk,a spy sabotages the laboratory. Banner falls in love with the spy, Jasmin, who performs missions only because her sister is being held hostage by Jasmin''s superiors. Banner and Jasmin try to escape from the enemy agents to rebuild their lives together, but the Hulk is never far from them.', '2778,2141,2820,2821,2822,2823,2800,2824,2825,2826,2827,2828,2829,2832,2833', '2778', '2808', '0', '1990-02-18', '68', '1,10,42', NULL, '95 Min', 'HD', 1, 0, 0, NULL, NULL, NULL, '4.6', 0, 1, 0, 0, 0, 1, '2019-04-04 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`videos_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `genre_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `videos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
