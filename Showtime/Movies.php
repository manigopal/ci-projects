<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Movies extends CI_Controller {
	function __construct()
	{
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_movie');
        //$this->load->model('Model_movie_genre');
        //$this->load->library("pagination");
    }

	public function index()
	{
		$data['general_settings'] = $this->Model_common->general_settings();
		$data['page_movies'] = $this->Model_common->page_movies();
		$data['social_settings'] = $this->Model_common->social_settings();

		$data['get_total_movies'] = $this->Model_movie->get_total_movies();
		/*$data['comment'] = $this->Model_common->all_comment();
		$data['social'] = $this->Model_common->all_social();
		$data['all_news'] = $this->Model_common->all_news();

		$data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();*/

		$this->load->library('pagination');

		$config = array();
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
	    $config['full_tag_close']   = '</ul></nav></div>';
	    $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	    $config['num_tag_close']    = '</span></li>';
	    $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
	    $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	    $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	    $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
	    $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	    $config['prev_tag_close']  = '</span></li>';
	    $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	    $config['first_tag_close'] = '</span></li>';
	    $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	    $config['last_tag_close']  = '</span></li>';

        $config["base_url"] = base_url() . "movies/page";
        $config["total_rows"] = $this->Model_movie->get_total_movies_count();
        $config['first_url'] = base_url() . 'movies';
        $config["per_page"] = 18;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        //$config['page_query_string'] = TRUE;

        $this->pagination->initialize($config);

        $offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['movies_fetched'] = $this->Model_movie->get_movies($config["per_page"], $offset);
        $data["links"] = $this->pagination->create_links();

        $this->load->view('view_header',$data);
		$this->load->view('view_movies',$data);
		$this->load->view('view_footer',$data);

	}

	public function view($id=0)
	{
		if( !isset($id) || !is_numeric($id) ) {
			redirect(base_url());
		}

		$tot = $this->Model_movie->movies_check($id);
		if(!$tot) {
			redirect(base_url());
		}

		$data['general_settings'] = $this->Model_common->general_settings();
		$data['page_movies'] = $this->Model_common->page_movies();
		$data['social_settings'] = $this->Model_common->social_settings();

		$data['get_total_movies'] = $this->Model_movie->get_total_movies();
		$data['movie_detail'] = $this->Model_movie->movie_detail($id);
		$data['sidebar_movie_detail'] = $this->Model_movie->sidebar_movie_detail();

		$this->load->view('view_header',$data);
		$this->load->view('view_movie',$data);
		$this->load->view('view_footer',$data);

	}

	/*public function genre($genre_type=NULL)
	{
		if( !isset($id) || !is_numeric($id) ) {
			redirect(base_url());
		}*

		$tot = $this->Model_movie_genre->genre_check($genre_type);
		if(!$tot) {
			redirect(base_url());
		}


		$data['general_settings'] = $this->Model_common->general_settings();
		$data['page_movies_genre'] = $this->Model_common->page_movies_genre();
		$data['social_settings'] = $this->Model_common->social_settings();
		
		$data['movies_by_genre'] = $this->Model_movie_genre->movies_by_genre($genre_type);
		$data['genre_by_name'] = $this->Model_movie_genre->genre_by_name($genre_type);

		//$data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

		$this->load->view('view_header',$data);
		$this->load->view('view_movies_genre',$data);
		$this->load->view('view_footer',$data);
	}*/

}