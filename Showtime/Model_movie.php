<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_movie extends CI_Model 
{
    public function get_total_movies()
    {
        $query = $this->db->query("SELECT * FROM movies_info");
        return $query->result_array();
    }

    public function get_total_movies_count() {
        $sql = 'SELECT * FROM movies_info';
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_movies($limit, $start) {
        $this->db->select('*');
        $this->db->from('movies_info');
        $this->db->limit($limit, $start);
        
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    /*public function all_categories()
    {
        $query = $this->db->query("SELECT * FROM tbl_category ORDER BY category_name ASC");
        return $query->result_array();
    }*/

    public function movies_check($id)
    {
        $sql = 'SELECT * FROM movies_info WHERE movie_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->num_rows();
    }

    public function movie_detail($id)
    {
        $sql = 'SELECT * FROM movies_info WHERE movie_id=?';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }

    public function sidebar_movie_detail()
    {
        $query = $this->db->query("SELECT * FROM movies_info ORDER BY rand()");
        return $query->result_array();
    }

    /*public function news_detail_with_category($id)
    {
        $sql = 'SELECT * 
                FROM tbl_news t1
                JOIN tbl_category t2
                ON t1.category_id = t2.category_id
                WHERE t1.news_id=?
                ORDER BY t1.news_id DESC';
        $query = $this->db->query($sql,array($id));
        return $query->first_row('array');
    }*/

    /*public function get_category_name_by_id($cat_id) {
        $sql = 'SELECT * FROM tbl_category WHERE category_id=?';
        $query = $this->db->query($sql,array($cat_id));
        return $query->first_row('array');
    }*/
}