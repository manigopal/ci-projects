
<!-- /w3l-medile-movies-grids -->
	<div class="general-agileits-w3l">
		<div class="w3l-medile-movies-grids">

				<!-- /movie-browse-agile -->
				
				      <div class="movie-browse-agile">
					     <!--/browse-agile-w3ls -->
						<div class="browse-agile-w3ls general-w3ls">
								<div class="tittle-head">
									<h4 class="latest-text">Movies </h4>
									<div class="container">
										<div class="agileits-single-top">
											<ol class="breadcrumb">
											  <li><a href="<?php echo base_url(); ?>">Home</a></li>
											  <li class="active" style="text-transform:Capitalize;">Movies</li>
											</ol>
										</div>
									</div>
								</div>
								     <div class="container">
							<div class="browse-inner">
								
							<?php 
							foreach($movies_fetched as $row) { 
							?>		
							   <div class="col-md-2 w3l-movie-gride-agile"> 
										 <a href="<?php echo base_url(); ?>movie/<?php echo $row->movie_id; ?>" class="hvr-shutter-out-horizontal">
										 	<img src="<?php echo base_url(); ?>/public/images/movies/<?php echo $row->movie_image; ?>" title="<?php echo $row->movie_name; ?>" alt=" " />
									     <div class="w3l-action-icon"><i class="fa fa-play-circle" aria-hidden="true"></i></div>
									</a>
									  <div class="mid-1">
										<div class="w3l-movie-text">
											<h6><a href="<?php echo base_url(); ?>movie/<?php echo $row->movie_id; ?>">
												<?php echo $row->movie_name; ?></a></h6>							
										</div>
										<div class="mid-2">
										
											<p><?php echo $row->release_year; ?></p>
											<div class="block-stars">
												<ul class="w3l-ratings">
													     
														 <li> 
														  <span>IMDB <i class="fa fa-star" aria-hidden="true"></i> 
														  	<?php echo $row->movie_rating; ?> 
														  </span>
														 </li>
										
												</ul>
											</div>
											<div class="clearfix"></div>
										</div>
											
									</div>
							 	    <div class="ribben two">
										<p>NEW</p>
									</div>	
									</div> 
								<?php } ?>
									

											<div class="clearfix"> </div>
								</div>
								
									<!-- /latest-movies1 -->
							   
								</div>
						</div>
				<!--//browse-agile-w3ls -->
						
						<!-- <div class="blog-pagenat-wthree"> -->
						<!-- <div class="row">
		                    <div class="col-md-12" style="margin-top:30px;text-align: center;"> -->
		                        <?php echo $links; ?>
		                    <!-- </div>
              			</div> -->
              			<!-- </div> -->

					</div>
				    <!-- //movie-browse-agile -->
				   <!--body wrapper start-->
				<!--body wrapper start-->
						
		</div>
	<!-- //w3l-medile-movies-grids -->
	</div>
	<!-- //comedy-w3l-agileits -->


	<!-- DataTables -->
<!-- <script src="admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script> -->

  <!-- DataTables -->
  <!-- <link rel="stylesheet" href="admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> -->
	
	<script>
	$(document).ready(function() {
    /* $('#test').DataTable( {
        "paging":   true,
        "ordering": false,
        "info":     false
    } ); */
} );
</script>

	