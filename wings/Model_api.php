<?php
  
  /*** THIS IS MODEL FILE ***/
  
  Class Model_api Extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	
	
//API call - update a customer
    public function update_customer($id, $data){
       $this->db->where('customer_id', $id);
       if($this->db->update('customers_info', $data)){
          return true;
        }else{
          return false;
        }
    }
	
	
}