Encrypt & Decrypt Password for Login, Register and Update API



I'm newbee to CI,

I'm trying to Create & Modify User Profile USING API (REST) json_encode option,

I want to use default encrypt & decrypt for password alone.,  for Login & Register...

I have added codes in particular files,


@  Config.php : 

$config['encryption_key'] = 'aQH76B8hIMuUUVY7ggu1Eo3Rg2KCiqeb';



@ Api.php in Controllers : // Customer Register

public function customer_register_api(){ 

            $path = $_FILES['customer_image']['name'];
            $path_tmp = $_FILES['customer_image']['tmp_name'];

            $final_name = $path;
                move_uploaded_file( $path_tmp, './public/images/customers/'.$final_name );
  
    
            $customer_name = $_POST['customer_name'];    
            $customer_image = $final_name;    
            $customer_address = $_POST['customer_address'];  
            $customer_city = $_POST['customer_city'];    
            $customer_state = $_POST['customer_state']; 
            $customer_pincode = $_POST['customer_pincode']; 
            $customer_phone_no = $_POST['customer_phone_no']; 
            $customer_email = $_POST['customer_email']; 
            $customer_password = md5($_POST['customer_password']); 
            $status = $_POST['status'];

           $customer_password_encrypt = $this->encryption->encrypt($customer_password);

            $customer_password_decrypt = $this->encryption->decrypt($customer_password_encrypt);

    if(!$customer_name || !$customer_image || !$customer_address || !$customer_city || !$customer_state || !$customer_pincode || !$customer_phone_no || !$customer_email || !$customer_password || !$status){ 
      //$this->response("Enter complete Product information to save", 400); 
        echo json_encode (
                    array(
                        "Status"=>'Failed',
                        "Error"=>'All Fields are Mandatory',
                    ));
    }else{ 
    
        $result = $this->Model_api->add_customer(array("customer_name"=>$customer_name, "customer_image"=>$customer_image, "customer_address"=>$customer_address, "customer_city"=>$customer_city, "customer_state"=>$customer_state, "customer_pincode"=>$customer_pincode, "customer_phone_no"=>$customer_phone_no, "customer_email"=>$customer_email, "customer_password"=>$customer_password, "status"=>$status)); 
      if($result === 0){ 
            //$this->response("Book information could not be saved. Try again.", 404); 
        echo json_encode (
                    array(
                        "Status"=>'Failed',
                        //"Seller Details"=>$query->result(),
                    ));
      }else{ 
            
            //$this->response("success", 200); 
        echo json_encode (
                    array(
                        "Status"=>'Success',
                        //"Seller Details"=>$result->result(),
                    ));
            }
        }

    }



@ Api.php in Controllers : // Customer Login

public function customer_login_api()
    {  
        //$seller_email = $_POST['seller_email'];
        $customer_phone_no = $_POST['customer_phone_no'];
        $customer_password = $_POST['customer_password'];
        
       //$seller_password = $this->encryption->encrypt($seller_password1);

        //$seller_password_encrypt = $this->encryption->encrypt($seller_password);

        $query = $this->db->query("SELECT customer_id,customer_name,customer_image,customer_address,customer_city,customer_state,customer_pincode,customer_phone_no,customer_email,status FROM `customers_info` WHERE `customer_phone_no` = '$customer_phone_no' AND `customer_password` = '$customer_password' AND `status` = 'Enable' ");
        $num = $query->num_rows();
        if($num > 0){
        echo json_encode (
                    array(
                        "Status"=>'Login Success',
                        "Customer Details"=>$query->result(),
                    ));
        }else{
            echo json_encode (
                    array(
                        "Status"=>'Login Failed',
                        "Error"=>'Invalid Credentials OR your account may be Inactive',
                    ));
        }
    }


@ Api.php in Controllers : // Customer Update

public function customer_update_api(){ 

            $path = $_FILES['customer_image']['name'];
            $path_tmp = $_FILES['customer_image']['tmp_name'];

            $final_name = $path;
                move_uploaded_file( $path_tmp, './public/images/customers/'.$final_name );
   
            $id = $_POST['customer_id'];
            $customer_name = $_POST['customer_name'];    
            $customer_image = $final_name;         
            $customer_address = $_POST['customer_address'];     
            $customer_city = $_POST['customer_city'];     
            $customer_state = $_POST['customer_state']; 
            $customer_pincode = $_POST['customer_pincode']; 
            $customer_phone_no = $_POST['customer_phone_no']; 
            $customer_email = $_POST['customer_email']; 
            $customer_password = $_POST['customer_password'];  
            $status = $_POST['status'];

            /*$id = $this->put('customer_id');
            $customer_name = $this->put('customer_name');    
            $customer_image = $final_name;         
            $customer_address = $this->put('customer_address');     
            $customer_city = $this->put('customer_city');     
            $customer_state = $this->put('customer_state'); 
            $customer_pincode = $this->put('customer_pincode'); 
            $customer_phone_no = $this->put('customer_phone_no'); 
            $customer_email = $this->put('customer_email'); 
            $customer_password = $this->put('customer_password');  
            $status = $this->put('status');*/

    /*if(!$customer_name || !$customer_address || !$customer_city || !$customer_state || !$customer_pincode || $customer_phone_no || !$customer_email || !$customer_password || !$status){*/ 

        if(!$customer_name){

       //$this->response("Enter complete Product information to save", 400); 
        echo json_encode (
                    array(
                        "Status"=>'Failed',
                        "Error"=>'All Fields are Mandatory',
                    ));
     }else{ 
    
        $result = $this->Model_api->update_customer($id,array("customer_name"=>$customer_name, "customer_image"=>$customer_image, "customer_address"=>$customer_address, "customer_city"=>$customer_city, "customer_state"=>$customer_state, "customer_pincode"=>$customer_pincode, "customer_phone_no"=>$customer_phone_no, 
            "customer_email"=>$customer_email, "customer_password"=>$customer_password, "status"=>$status)); 
       if($result === 0){ 
           // $this->response("Book information could not be saved. Try again.", 404); 
        echo json_encode (
                    array(
                        "Status"=>'Failed',
                        //"Seller Details"=>$query->result(),
                        //$this->response->statusCode(200),
                    ));
                    //$this->response->statusCode(200);
       }else{ 
            
            //$this->response("success", 200); 
        echo json_encode (
                    array(
                        "Status"=>'Success',
                        //"Seller Details"=>$result->result(),
                    ));
            }
        }
 
    }



@ Model_api.php in Model : // Customer Register

<?php
  
  /*** THIS IS MODEL FILE ***/
  
  Class Model_api Extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	
	
//API call - ADD customer
    //API call - add new customer
    public function add_customer($data){
        if($this->db->insert('customers_info', $data)){
           return true;
        }else{
           return false;
        }
    }


@ Model_api.php in Model : // Customer Update

//API call - update a customer
    public function update_customer($id, $data){
       $this->db->where('customer_id', $id);
       if($this->db->update('customers_info', $data)){
          return true;
        }else{
          return false;
        }
    }    
	
	
}



--------------------------------------------------------------------------------------------------------------------------------------------


when i use 

$customer_password_encrypt = $this->encryption->encrypt($customer_password);

encrypt is working., 

i use customer_password => varchar(99) at DB, (Not sure 99 is enough or need to increase)

On simple words,
 I want to login, register & update with password encrypt & decrypt., only for the field #customer_password